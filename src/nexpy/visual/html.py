from IPython.display import HTML
from ..global_settings import ARRAY


"""Visualiation through HTML."""


def matrix(matrix: ARRAY, width = 500):

    """ Create a HTML table from a matrix.
    
    Args:
        matrix (ARRAY): 2D numpy array representing the matrix to be plotted.
        width (int, optional): Width of the HTML table. Defaults to 500.
    
    Returns:
        HTML: HTML table."""

    html_content = """
    <!DOCTYPE html>
    <html>
        <head>
            <style>
                /* Style the table to set a fixed width */
                table {
                    table-layout: fixed;
                    width: """ + str(width) + """px; /* Adjust the width as needed */
                    border-collapse: collapse;
                }

                /* Style the table header */
                th {
                    background-color: #f2f2f2;
                }

                /* Style specific table cells with a background color */
                td.block1 {
                    background-color: #ffcccc; /* Red */
                }

                td.block2 {
                    background-color: #ccffcc; /* Green */
                }

                td.block3 {
                    background-color: #ccccff; /* Blue */
                }

                /* Set the table to be scrollable in both X and Y directions */
                .table-container {
                    width: 100%;
                    height: 440px; /* Adjust the height as needed */
                    overflow: auto;
                }
                </style>
        </head>
        <body>
            <div class="table-container">"""

    html_content += "<table>"

    for itr in matrix:
        html_content += "<tr>"
        
        for iti in itr:
            html_content +="<td><center>"
            html_content += str(iti)
            html_content += "</center></td>"
        
        html_content += "</tr>"
        
        
    html_content += """
                </table>
            </div>
        </body>
    </html>
    """

    return HTML(html_content)


def progress_bar(progress):

    """Display a progress bar.
    
    Args:
        progress (float): Progress in percent.
        
    Returns:
        HTML: HTML progress bar."""
    
    return HTML("""
                <div style='background-color:lightgray;height:25px;width:100%'>
                    <p style='color:white;text-align:center;position:absolute;left:50%;top:-4px;'>
                        <b>""" + str(progress) + """ %</b>
                    </p>
                    <div style='background-color:green;height:25px;width:""" + str(progress) +"""%'></div>
                </div>""")
