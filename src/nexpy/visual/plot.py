import numpy as np
import matplotlib.pyplot as plt

from ..utils import array_ratio
from matplotlib import colors, gridspec
from ..global_settings import ARRAY, EXP_PLOT_STYLE, MARKER_STYLE


"""Visualiation through plots."""


def matrix(matrix, positions, fontsize=8, display_title = True, display_values = True, dark_mode = False):

    """ Plot a matrix with submatrices defined by positions along the x-axis and y-axis.
    
    Args:
        matrix (ARRAY): 2D numpy array representing the matrix to be plotted.
        positions (list): List of indices indicating the submatrix positions along the x-axis and y-axis.
        fontsize (int, optional): Font size. Defaults to 8.
        display_title (bool, optional): Display title. Defaults to True.
        display_values (bool, optional): Display values. Defaults to True.
        dark_mode (bool, optional): Dark mode. Defaults to False.
    """

    # Create a figure with two subplots

    fig, axs = plt.subplots(1, 2, figsize=(12, 4), sharey=True, dpi=150)

    # Dark mode

    if dark_mode:

        fig.set_facecolor('#373E4B')

    # Plot the original matrix

    _matrix(matrix, positions, axs[0], fontsize, display_values = display_values, display_title = display_title, dark_mode = dark_mode)

    # Plot the absolute value of the matrix

    _matrix(matrix, positions, axs[1], fontsize, abs_val=True, display_values = display_values, display_title = display_title, dark_mode = dark_mode)

    plt.tight_layout()

    plt.show()


def big_matrix(matrix: ARRAY, font_size = 8, abs_val = True, display_title = False, display_colorbar = False, dark_mode = False):
    
    """ Plot a big matrix.

    Args:
        matrix (ARRAY): 2D numpy array representing the matrix to be plotted.
        font_size (int, optional): Font size. Defaults to 8.
        abs_val (bool, optional): Plot absolute value. Defaults to True.
        display_title (bool, optional): Display title. Defaults to False.
        display_colorbar (bool, optional): Display colorbar. Defaults to False.
        dark_mode (bool, optional): Dark mode. Defaults to False.
    """

    # Create a figure

    fig, ax = plt.subplots(figsize=(10, 6))

    # Dark mode

    if dark_mode:

        fig.set_facecolor('#373E4B')

    # Plot the big matrix

    _matrix(matrix, None, ax, font_size, abs_val = abs_val, display_values = False, display_title = display_title, display_colorbar = display_colorbar, dark_mode = dark_mode)

    plt.tight_layout()

    plt.show()


def _matrix(matrix, positions, ax, fontsize, abs_val=False, display_values=True, display_title = True, display_colorbar = True, dark_mode = False):
    
    """Plot a matrix with submatrices defined by positions along the x-axis and y-axis.
    
    Args:
        matrix (ARRAY): 2D numpy array representing the matrix to be plotted.
        positions (list): List of indices indicating the submatrix positions along the x-axis and y-axis.
        ax (plt.axes): Axes object.
        fontsize (int): Font size.
        abs_val (bool, optional): Plot absolute value. Defaults to False.
        display_values (bool, optional): Display values. Defaults to True.
        display_title (bool, optional): Display title. Defaults to True.
        display_colorbar (bool, optional): Display colorbar. Defaults to True.
        dark_mode (bool, optional): Dark mode. Defaults to False.
    """
    """

    Plot a matrix with submatrices defined by tilings along the x-axis.

    Parameters:

    - matrix: 2D numpy array representing the matrix to be plotted.

    - tiling_indices_x: List of indices indicating the tiling positions along the x-axis.

    """

    # Set title of the matrix

    num_nonzero = matrix[matrix.nonzero()].size

    num_unique = np.unique(matrix).size

    title = ""

    if display_title:

        title = f"abs_val = {abs_val}, dim = {matrix.shape[0]} x {matrix.shape[1]} \n"

        if abs_val:

            title += f"nnz = {num_nonzero}, nnz frc. = {num_nonzero/matrix.size:.2f}"

        else:

            title += f" nu = {num_unique}, nu frc. = {num_unique/matrix.size:.2f}"

    ax.set_title(title)

     # Create a custom colormap

    if dark_mode:

        cmap = colors.LinearSegmentedColormap.from_list('custom', [(0, '#373E4B'), (1, 'white')], N = 256)

    else:

        cmap = colors.LinearSegmentedColormap.from_list('custom', [(0, 'white'), (1, 'red')], N = 256)

    # Create a heatmap of the matrix

    im = None

    if abs_val:

        im = ax.imshow(np.abs(matrix), cmap=cmap, interpolation='nearest')

    else:

        im = ax.imshow(matrix, cmap=cmap, interpolation='nearest')

    # Set the line width for the x-axis and y-axis

    ax.spines['bottom'].set_linewidth(1)

    ax.spines['left'].set_linewidth(1)

    ax.spines['top'].set_linewidth(1)

    ax.spines['right'].set_linewidth(1)


    # Calculate the number of rows and columns in the matrix

    num_rows, num_cols = matrix.shape

    if not positions is None:

        # Shift tiling positions

        positions = [idx - 0.5 for idx in positions]

        # Draw rectangles to highlight the submatrices along the x-axis
        
        for i in range(len(positions) - 1):

            x_start = positions[i]

            x_end = positions[i + 1]

            rect = plt.Rectangle((x_start, -1), x_end - x_start, num_rows + 2, linewidth=1, edgecolor='black', facecolor='none')

            ax.add_patch(rect)

        # Draw rectangles to highlight the submatrices along the y-axis

        for i in range(len(positions) - 1):

            y_start = positions[i]

            y_end = positions[i + 1]

            rect = plt.Rectangle((-1, y_start), num_cols + 2, y_end - y_start, linewidth=1, edgecolor='black', facecolor='none')

            ax.add_patch(rect)

    # Display colorbar

    if display_colorbar:

        cbar = plt.colorbar(im, ax=ax)

    # Add matrix values if activated

    if display_values:

        len_geo = np.sqrt(len(matrix)*len(matrix[0]))

        for i in range(len(matrix)):

            for j in range(len(matrix[0])):

                val = 0.0

                if abs_val:

                    val = abs(matrix[i][j])

                else:

                    val = matrix[i][j]

                x = j

                y = i

                text = ""

                if len_geo <= 3:

                    text = f"{val:.3f}"

                elif len_geo <= 10:

                    text = f"{val:.2f}"

                elif len_geo <= 15:

                    text = f"{val:.1f}"

                elif len_geo <= 20:

                    text = f"{val:.0f}"

                text = f"{val:.0f}"

                ax.text(x, y, text, va='center', ha='center', fontsize=fontsize)

    if not positions is None:

        # Set labels on x and y axes based on tiling positions

        ax.set_xticks(positions)

        ax.set_yticks(positions)

        ax.set_xticklabels([int(idx+0.5) for idx in positions])

        ax.set_yticklabels([int(idy+0.5) for idy in positions])

        # Display x-axis labels on top

        ax.xaxis.set_ticks_position('top')


def experiment(xticks: np.array, plots, title, xlabel, ylabel, xlabelrot=0, ylogscale=False, xlogscale=False, markers=True, fontsize=12, max_ratio=True):

    """ Plot experiment data.
    
    Args:
        xticks (np.array): x-axis ticks.
        plots (list): List of dictionaries with data and labels.
        title (str): Plot title.
        xlabel (str): x-axis label.
        ylabel (str): y-axis label.
        ylogscale (bool, optional): Log scale for y-axis. Defaults to False.
        xlogscale (bool, optional): Log scale for x-axis. Defaults to False.
        markers (bool, optional): Plot markers. Defaults to True.
        fontsize (int, optional): Font size. Defaults to 12.
        max_ratio (bool, optional): Plot max-ratio. Defaults to True.
    
    Returns:
        None: Nothing to return."""

    # Set plot style

    plt.style.use(EXP_PLOT_STYLE)

    # Create a figure with custom subplot height ratios
    
    fig = plt.figure(figsize=(7.5, 2.5), dpi=150)

    gs = gridspec.GridSpec(1, 2, width_ratios=[3, 2], hspace=0.5)

    # Left side sub plot

    plt.subplot(gs[0])

    # Plot data and standard deviations

    for i_plot, plot in enumerate(plots):

        if markers:

            plt.plot(xticks, plot['data'], label=plot['label'], marker=MARKER_STYLE[i_plot])
        
        else:

            plt.plot(xticks, plot['data'], label=plot['label'])

        var_keys_exist = ('sigma_lower' in plot) & ('sigma_upper' in plot)

        if var_keys_exist:

            plt.fill_between(xticks, plot['sigma_lower'], plot['sigma_upper'], alpha=0.15)

    # Set title

    plt.title(title, loc = 'center', fontsize=fontsize)

    # Set x, y labels

    plt.xlabel(xlabel, fontsize=fontsize)

    plt.ylabel(ylabel, fontsize=fontsize)

    # Set font size for ticks
    
    plt.xticks(fontsize=fontsize, rotation=xlabelrot)
    
    plt.yticks(fontsize=fontsize)

    # Set log scale

    if ylogscale:

        plt.yscale('log')

        pass

    if xlogscale:

        plt.xscale('log')

    # Legend and grid

    plt.legend(fontsize=fontsize)

    plt.grid(linestyle='dotted', linewidth=1)

    if max_ratio:

        # Check if there is more than one plot

        if len(plots) <= 1:

            raise Exception("Sorry, but in order to compute the max-ratio you need at least two data sets.")

        # Right side sub plot

        plt.subplot(gs[1])

        # Compute and plot the max-ratio of data given by plots[0] and plots[1:]

        distorted_ratios = [array_ratio(plots[0]['data'], plot['data']) for plot in plots[1:]]

        max_distorted_ratio = np.amax(distorted_ratios, axis=0)

        if markers:
            
            plt.plot(xticks, max_distorted_ratio, label = f"max-ratio", marker="x")
        
        else:

            plt.plot(xticks, max_distorted_ratio, label = f"max-ratio")

        # Set title

        plt.title(f'max-ratio', loc = 'center', fontsize=fontsize)

        # Set x, y labels

        plt.xlabel(xlabel, fontsize=fontsize)

        #plt.ylabel("units", fontsize="12")
        
        # Set font size for ticks
        
        plt.xticks(fontsize=fontsize, rotation=xlabelrot)
        
        plt.yticks(fontsize=fontsize)

        # Set log scale

        if xlogscale:

            plt.xscale('log')

        # Legend and grid

        plt.legend(fontsize=fontsize)

        plt.grid(linestyle='dotted', linewidth=1)

    # Show plot

    plt.show()
