import numpy as np
import minterpy as mp

from nexpy.core.triangular.integer_list import IntegerList
from nexpy.global_settings import ARRAY, FLOAT_DTYPE, INT_DTYPE
from nexpy.core.triangular.ltfbm import LowerTriangularFactorisedBlockMatrix
from nexpy.utils import get_exponent_matrix_iterative, chebychev_2nd_order
from nexpy.jit_compiled_utils import leja_order, dds_1_dimensional

class FastNewtonPolynomial:

    """This is the FastNewtonPolynomial class."""

    def __init__(self, spatial_dimension : INT_DTYPE, poly_degree : INT_DTYPE, p_degree: FLOAT_DTYPE = 2.0):

        """Constructor for FastNewtonPolynomial object.

        Args:
        -----
            spatial_dimension: INT_DTYPE
                Spatial dimension.

            poly_degreee: INT_DTYPE
                 Polynomial degree.

            p_degree: FLOAT_DTYPE
                Degree of the lp space.
        
        Notes:
        ------
        - TODO We are planning to implement the FastNewtonPolynomial class in such a way that it does not need to use the exponents matrix anymore.
        - TODO This will include to rewrite the `np.take_along_axis` function in the __init__ method. """

        # Set attributes

        self._spatial_dimension = spatial_dimension
        self._poly_degree = poly_degree
        self._p_degree = p_degree

        # ! NOTE ! : The following two usages of the 'get_exponent_matrix_iterative' will be reduced to one
        #            usage in the future!
        #            The first usage is for the exponents and the second usage is for the tiling.
        #            Also, both could be done in the same function call.
        #            But in future, we will only use the tiling and the won't be computed at all.

        # Compute exponents

        self._exponents = get_exponent_matrix_iterative(
            spatial_dimension = spatial_dimension,
            poly_degree = poly_degree,
            p_degree = p_degree)

        # Initialise tiling

        # Alternative: self._tiling = extract_tiling_from_exponent_matrix(self._exponents)
    
        self._tiling = IntegerList(get_exponent_matrix_iterative(
            spatial_dimension = spatial_dimension,
            poly_degree = poly_degree,
            p_degree = p_degree,
            tilings_only = True
        ))

        # Compute unisolvent nodes

        cheb_points = chebychev_2nd_order(poly_degree + 1)[::-1]

        leja_cheb_points = cheb_points[leja_order(cheb_points)]
        
        self._gen_points = np.array([leja_cheb_points for _ in range(spatial_dimension)]).T

        self._unisolvent_nodes = np.take_along_axis(self._gen_points, self._exponents, axis=0) 

        # Initialise l2n

        self._l2n = self._init_l2n()

        # Initialise coeffs with zero vector

        self._coeffs = np.zeros((self._tiling.size), dtype = FLOAT_DTYPE)

    @property
    def spatial_dimension(self) -> INT_DTYPE:

        """Property method for the spatial dimension."""

        return self._spatial_dimension

    @property
    def poly_degree(self) -> INT_DTYPE:

        """Property method for the polynomial degree."""

        return self._poly_degree
    
    @property
    def unisolvent_nodes(self) -> ARRAY:

        """Property method for the unisolvent nodes."""

        return self._unisolvent_nodes

    @property
    def tiling(self) -> IntegerList:

        """Property method for the tiling."""

        return self._tiling

    @property
    def exponents(self) -> ARRAY:

        """Property method for the exponents."""

        return self._exponents

    @property
    def gen_points(self) -> ARRAY:

        """Property method for the generated points."""

        return self._gen_points
    
    @property
    def unisolvent_nodes(self) -> ARRAY:

        """Property method for the unisolvent nodes."""

        return self._unisolvent_nodes

    @property
    def l2n(self) -> LowerTriangularFactorisedBlockMatrix:

        """Property method for the Lagrange to Newton transformation"""

        return self._l2n

    @property
    def coeffs(self) -> ARRAY:

        """Property method for the coefficients of the polynomial."""

        return self._coeffs
    
    @coeffs.setter
    def coeffs(self, coeffs: ARRAY):
            
        """ Setter for the coefficients property. """

        self._coeffs = coeffs

    def _init_l2n(self):

        """Initialise the Lagrange to Newton transformation.""" 

        # Access class attributes

        til = self._tiling

        gen_points = self._gen_points

        spatial_dimension = self._spatial_dimension

        # Compute l2n

        ref = til.item(0)
        
        l2n_fp = np.eye(ref, dtype = FLOAT_DTYPE)
        
        gen_vals = gen_points[:, 0]
        
        dds_1_dimensional(gen_vals, l2n_fp) # uses result placeholder

        # Built ltfbm

        depth = spatial_dimension

        l2n = None

        for idd in range(1, depth):

            if idd == 1:

                node = l2n_fp

            else:

                node = l2n.root

            l2n = LowerTriangularFactorisedBlockMatrix(
                first_piece = l2n_fp,
                node = node,
                tiling = til)

        return l2n

    def interpolate(self, func : callable) -> ARRAY:

        """ Interpolate a function on unisolvent nodes. 
        
        Args:
        -----
            func: callable
                Function which is going to be interpolated.
            
        Returns:
        --------
            ARRAY: 
                The coefficients computed for the fast Newton polynomial."""

        # Extract sample

        sample = func(self._unisolvent_nodes)

        deep_compatible = self.spatial_dimension > 2

        # Apply l2n

        self._coeffs = self._l2n.dot(
            vec = sample,
            deep_compatible = deep_compatible)

        # Return coefficients

        return self._coeffs
  
    def evaluate(self) -> ARRAY:

        """Evaluate the polynomial at the unisolvent nodes. 

        Returns:
        --------
            ARRAY: 
                The polynomial evaluated at the unisolvent nodes.
        
        Notes:
        ------
        - NOTE Currently utiilizes minterpy."""

        # Compute n2l

        ref = self._tiling.item(0)

        mis = mp.MultiIndexSet(np.arange(ref).reshape(-1, 1))

        grd = mp.Grid(mis)

        points = grd.unisolvent_nodes[:ref, :]

        points_1d = points

        n2l_fp = mp.utils.eval_newton_monomials(
            points_1d,
            mis.exponents,
            points,
            triangular=True,
        )

        # Built ltfbm

        depth = self._spatial_dimension

        n2l = None

        for idd in range(1, depth):

            if idd == 1:

                node = n2l_fp

            else:

                node = n2l.root

            n2l = LowerTriangularFactorisedBlockMatrix(
                first_piece = n2l,
                node = node,
                tiling = self._tiling)

        # Apply n2l and return

        return n2l.dot(
            vec = self._coeffs,
            fast_mode = True)

    def __call__(self, grid: ARRAY) -> ARRAY:

        """Evaluate the polynomial at the given grid.

        Args:
        -----
            grid: ARRAY
                Grid at which to evaluate the polynomial.

        Returns:
        --------
            ARRAY:
                The polynomial evaluated at the given grid.
        
        Notes:
        ------
        - NOTE Currently utilizes minterpy.
        - TODO We are planning to implement the __call__ method in such a way that it does not need to use the exponents matrix anymore."""

        return mp.utils.eval_newton_polynomials(
            grid,
            self._coeffs,
            self._exponents.astype(np.int32),
            self._gen_points)

if __name__ == "__main__":

    pass