import numpy as np

from numba import njit
from nexpy.global_settings import ARRAY, INT_DTYPE, FLOAT_DTYPE

"""This module contains the jit-compiled utility functions of the NexPy package. 

Notes:
------
- NOTE: The following functions are not (yet) included in the tests."""

@njit
def get_exponent_matrix_iterative_subroutine(spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE) -> ARRAY:

    """ Compute the exponent matrix.

    Args:
    -----
        spatial_dimension: INT_DTYPE
            The spatial dimension of the polynomial.
        
        poly_degree: INT_DTYPE
            The degree of the polynomial.
        
        p_degree: FLOAT_DTYPE
            The parameter of the l^p norm.
        
    Returns:
    --------
        ARRAY
            The exponent matrix of the specified parameters.
    
    Notes:
    ------
    - NOTE: Only recommended for p_degree between 0 and 2."""

    multi_index = np.zeros(spatial_dimension, dtype=INT_DTYPE)

    # Get memory allocation

    memory_allocation = _get_exponent_matrix_memory_allocation(
        spatial_dimension = spatial_dimension,
        poly_degree = poly_degree,
        p_degree = p_degree
    )

    # Allocates spatial_dimension*memory_allocation entries

    exponents = np.zeros((memory_allocation, spatial_dimension), dtype=INT_DTYPE)

    n_p = poly_degree**p_degree

    num_p = np.arange(0, poly_degree + 1).astype(FLOAT_DTYPE)**p_degree

    multi_index_p = np.zeros(spatial_dimension, dtype=FLOAT_DTYPE)

    sum_multi_index_p = 0.0

    N, j = 0, 0

    stop = False

    while(not stop):

        # Check if the mult-index has p degree smaller or equal than n=poly_degree

        if sum_multi_index_p <= n_p:

            exponents[N] = multi_index

            N += 1 # Increase the total count by one

            j = 0 # Set the carry to zero and continue the exploring route

        else:

            # !!! CAUTION THERE MAY EXIST A ROUND OFF ERROR !!!

            sum_multi_index_p = np.sum(multi_index**p_degree)

            if sum_multi_index_p <= n_p:

                exponents[N] = multi_index

                N += 1 # Increase the total count by one

                j = 0 # Set the carry to zero and continue the exploring route

            else:

                # Increase the carry by one : handle multi-index

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] = 0

                multi_index_p[j] = 0

                j+=1

        while(True):

            if j >= spatial_dimension:

                # Exceeding the dimension forces the algorithm to stop

                stop = True
                
                break

            elif multi_index[j] < poly_degree:

                # Increase the multi-index at the j-th position by one.

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] += 1

                multi_index_p[j] = num_p[multi_index[j]]

                sum_multi_index_p = sum_multi_index_p + multi_index_p[j]

                # Do ->not<- set the carry to zero. This job is done by the p degree condition

                break

            else:

                # Increase the carry by one : handle multi index

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] = 0

                multi_index_p[j] = 0

                j += 1

    return exponents[0:N]

@njit
def get_exponent_matrix_iterative_tilings_only_subroutine(spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE) -> ARRAY:

    """Computes the tilings of the exponent matrix.

    Args:
    -----
        spatial_dimension: INT_DTYPE
            The spatial dimension of the polynomial.

        poly_degree: INT_DTYPE
            The degree of the polynomial.
        
        p_degree: FLOAT_DTYPE
            The parameter of the l^p norm.
    
    Returns:
    --------
        ARRAY:
            The tilings of the exponent matrix of the specified parameters.
    
    Notes:
    ------
    - NOTE: Only recommended for p_degree between 0 and 2."""

    multi_index = np.zeros(spatial_dimension, dtype=INT_DTYPE)

    # Get memory allocation

    memory_allocation = _get_exponent_matrix_memory_allocation(
        spatial_dimension = spatial_dimension - 1,
        poly_degree = poly_degree,
        p_degree = p_degree
    )

    # Allocates only memory_allocation entries with memory_allocation being smaller 

    tiling = np.zeros(memory_allocation, dtype=INT_DTYPE)

    n_p = poly_degree**p_degree

    num_p = np.arange(0, poly_degree + 1).astype(FLOAT_DTYPE)**p_degree

    multi_index_p = np.zeros(spatial_dimension, dtype=FLOAT_DTYPE)

    sum_multi_index_p = 0.0

    k, l, j = 0, 0, 0

    stop = False

    while(not stop):

        # Check if the mult-index has l^p degree smaller or equal than n=poly_degree

        if sum_multi_index_p <= n_p:

            k += 1 # Increase the tiling count by one

            j = 0 # Set the carry to zero and continue the exploring route

        else:
            # !!! CAUTION THERE MAY EXIST A ROUND OFF ERROR !!!

            sum_multi_index_p = np.sum(multi_index**p_degree)

            if sum_multi_index_p <= n_p:

                k += 1 # Increase the tiling count by one

                j = 0 # Set the carry to zero and continue the exploring route

            else:

                # Increase the carry by one : handle multi-index

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] = 0

                multi_index_p[j] = 0

                j+=1

            # Increase the carry by one : handle tiling

            if k > 0:

                tiling[l] = k

                l += 1

            k = 0

        while(True):

            if j >= spatial_dimension:

                # Exceeding the dimension forces the algorithm to stop

                stop = True
                
                break

            elif multi_index[j] < poly_degree:

                # Increase the multi-index at the j-th position by one.

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] += 1

                multi_index_p[j] = num_p[multi_index[j]]

                sum_multi_index_p = sum_multi_index_p + multi_index_p[j]

                # Do ->not<- set the carry to zero. This job is done by the l^p degree condition

                break

            else:

                # Increase the carry by one : handle multi index

                sum_multi_index_p = sum_multi_index_p - multi_index_p[j]

                multi_index[j] = 0

                multi_index_p[j] = 0

                j+=1

                # Increase the carry by one : handle tiling

                if k > 0:

                    tiling[l] = k

                    l +=1

                k = 0

    return tiling[0:l]

@njit
def _get_exponent_matrix_memory_allocation(spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE) -> INT_DTYPE:

    """ Computes the memory allocation for the exponent matrix.

    Args:
    -----
        spatial_dimension: INT_DTYPE
            The spatial dimension of the polynomial.
        
        poly_degree: INT_DTYPE
            The degree of the polynomial.
        
        p_degree: FLOAT_DTYPE
            The parameter of the l^p norm.

    Returns:
    --------
        INT_DTYPE
            The memory allocation needed for the exponent matrix."""

    if p_degree <= 1.0:
        singular = 1 + spatial_dimension * poly_degree

        if spatial_dimension < poly_degree:
            return INT_DTYPE(singular*(1-p_degree) + _binomial(poly_degree + spatial_dimension, spatial_dimension)*p_degree)

        else:
            return INT_DTYPE(singular*(1-p_degree) + _binomial(poly_degree + spatial_dimension, poly_degree)*p_degree)

    elif p_degree <= 2.0:
        fac1 = (p_degree * np.e / spatial_dimension)**(1/p_degree)
        fac2 = np.sqrt(p_degree / (2 * np.pi * spatial_dimension))
        
        return INT_DTYPE(np.ceil((fac1 * (poly_degree + 2) * np.math.gamma(1 + 1 / p_degree))**spatial_dimension * fac2))

    else:
        return INT_DTYPE((poly_degree + 1)**spatial_dimension)

@njit    
def extract_tiling_from_exponent_matrix_subroutine(lst: list) -> ARRAY:

    """Extract the tilings from the exponent matrix.
    
    Args:
    -----
        lst: list
            The exponent matrix.
    
    Returns:
    --------
        ARRAY
            The tilings of the exponent matrix.
            
    Notes:
    ------
    - NOTE This function has a linear time complexity."""

    counter = 0
    res = np.empty(0, dtype=INT_DTYPE)

    for itr_l in lst:
        if itr_l:
            counter += 1
        else:
            if not counter == 0:
                res = np.append(res, counter)
            counter = 1 
    res = np.append(res, counter)

    return res

@njit
def _binomial(n: INT_DTYPE, m: INT_DTYPE) -> INT_DTYPE:

    """ Computes the binomial coefficient n choose m .

    Args:
    -----
        n: INT_DTYPE
            The first parameter of the binomial coefficient.
        
        m: INT_DTYPE
            The second parameter of the binomial coefficient.

    Returns:
    --------
        INT_DTYPE
            The binomial coefficient n choose m."""

    if m < 0 or m > n:
        return 0

    result = 1

    for i in range(min(m, n - m)):
        result = result * (n - i) // (i + 1)

    return result

@njit
def leja_order(nodes):
    
    """Leja Order of Nodes.

    Args:
    -----
        Nodes: ARRAY
            The nodes to be ordered.

    Returns:
    --------
        ARRAY
            The order which is the numerically the best.

    Notes:
    ------
    - NOTE This function originates from Minterpy.
    - NOTE: This function is currently only used in the fast Newton polynomial class."""

    n = len(nodes) - 1
    ord = np.arange(1, n + 1, dtype=INT_DTYPE)
    lj = np.zeros(n + 1, dtype=INT_DTYPE)
    lj[0] = 0
    m = 0

    for k in range(0, n):
        jj = 0
        for i in range(0, n - k):
            p = 1
            for j in range(k + 1):
                p = p * (nodes[lj[j]] - nodes[ord[i]])
            p = np.abs(p)
            if p >= m:
                jj = i
                m = p
        m = 0
        lj[k + 1] = ord[jj]
        ord = np.delete(ord, jj)

    return lj

@njit
def dds_1_dimensional(grid_values: ARRAY, result_placeholder: ARRAY) -> None:

    """ One dimensional divided difference scheme.

    Args:
    -----
        grid_values: ARRAY
            The positions of the interpolation nodes respectively.

        result_placeholder: ARRAY
            2D array initially containing the function values.

    Returns:
    --------
        None
            The Newton coefficients corresponding to the interpolating 1D polynomial(s).

    Notes:
    ------
    - NOTE This function originates from Minterpy.
    - NOTE This function is currently only used in the fast Newton polynomial class.
    - NOTE based on:
        https://en.wikipedia.org/wiki/Divided_differences
        https://stackoverflow.com/questions/14823891/newton-s-interpolating-polynomial-python"""

    c = result_placeholder 
    n = len(c)
    v = grid_values[:n]   
    for i in range(1, n):
        i_prev = i - 1
        coeff_slice = c[i:]
        val_slice = v[i:]
        val_diff = val_slice - v[i_prev]
        val_diff = np.expand_dims(val_diff, -1)
        coeff_diff = coeff_slice - c[i_prev]
        coeff_slice[:] = coeff_diff / val_diff

@njit
def jit_concatenate_2d(a, b):
    
    """Concatenate two 2D arrays.

    Args:
    -----
        a: ARRAY
            The first array.

        b: ARRAY
            The second array.

    Returns:
    --------
        ARRAY
            The concatenated two dimensional array.
    
    Notes
    -----
    - NOTE: This method is currently only used in the jit_ltfbm_reconstruction method."""

    # Create an empty array with the correct shape and dtype
    if a.shape[1] == b.shape[1]:
        c = np.empty((a.shape[0] + b.shape[0], a.shape[1]), dtype=a.dtype)

        # Copy the data from the input arrays into the empty array
        for i in range(a.shape[0]):
            c[i] = a[i]

        for i in range(b.shape[0]):
            c[i + a.shape[0]] = b[i]

        return c

if __name__ == "__main__":

    pass