import numpy as np

""" This module contains the global settings of the NexPy package. """

# Typing

INT_DTYPE = np.int64

FLOAT_DTYPE = np.float64

ARRAY = np.ndarray

# Styling

EXP_PLOT_STYLE = "bmh"

MARKER_STYLE = ['o', 's', '^', 'D', 'v', 'p', '*', '+', 'x']

# Testing

def EPS(x: ARRAY, y: ARRAY): return np.max(np.abs(x - y))

EPS_HIGH = 10**-18

EPS_MID = 10**-14

EPS_LOW = 10**-10

if __name__ == "__main__":

    pass