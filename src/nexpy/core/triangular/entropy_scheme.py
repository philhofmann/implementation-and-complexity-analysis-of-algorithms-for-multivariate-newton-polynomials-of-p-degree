import numpy as np

from numba import int64
from numba.experimental import jitclass
from nexpy.global_settings import INT_DTYPE, ARRAY

""" entropy_scheme.py module

Notes:
------
- TODO Doc"""

spec = [
    ('_cs', int64[:]), # cumulative sum
    ('_es', int64[:, :]), # entropies 
    ('_ee', int64) # entropy entry
]

@jitclass(spec)
class EntropyScheme(object):

    """ The EntropyScheme jitclass.

    The EntropyScheme is a class that computes the entropies of a tiling efficiently and stores them.
    
    Attributes:
    -----------
        _cs(ARRAY): The cumulative sum of the tiling.
        _es(ARRAY): The entropies of the tiling.
        _ee(INT_DTYPE): The current entropy entry.

    Methods:
    --------
        __init__(): Initialises the EntropyScheme object.
        init(tiling: ARRAY): Initialises the EntropyScheme object.
        projection(i: INT_DTYPE): Returns the i-th projection of the entropy scheme.
        depth_projection(): Returns the depth projection of entropy scheme.
        next(): Computes the next entropies/entropy scheme using the Gamma-Iteration.
    
    Notes:
    ------
    - Exceptions can not be raised in numba jitclass."""
    
    def __init__(self):

        """Initialises the EntropyScheme object.
       
       Notes:
       -----
       The init method is outsourced due to technical reasons."""
        
        pass

    def init(self, tiling: ARRAY):

        """ Initialises the EntropyScheme object.
        
        Args:
        -----
            tiling(ARRAY): The tiling.
        
        Notes:
        -----
        - The tiling is a 1D array of integers."""
        
        # Initialise cumulative sum
        self._cs = np.cumsum(tiling).astype(INT_DTYPE)
        
        # Initialise entropies
        self._es = np.ones((self._cs[-1]), dtype = INT_DTYPE).reshape(-1, 1)

        # Initialise current entropy entry with negative one
        self._ee = -1
        
    @property
    def cumsum(self) -> ARRAY:

        """Property method for the cumulative sum of the tiling.
        
        Returns:
        --------
            ARRAY: The cumulative sum of the tiling."""
        
        return self._cs
    
    @property
    def entropies(self) -> ARRAY:
        
        return self._es

    @property
    def entropy_entry(self) -> INT_DTYPE:

        return self._ee
    
    def projection(self, i: INT_DTYPE):
        
        """
            
            NOTE: Returns for i = 0 the so called flat tiling in each step

            TODO: Add return type
            
        """
        
        return self._es.T[i]
    
    def depth_projection(self):

        """
        
            TODO: Add return type, optimise the way this is stored. Much redundancy.
        
        """

        # Accessing class attributes
        es = self._es
        ee = self._ee

        # Initialise depths
        depths = np.zeros((ee), dtype = INT_DTYPE)

        for idx_e, itr_e in enumerate(es):

            if np.all(itr_e == 1):
                depths[idx_e] = 1 # OBSERVE

            else:
                depths[idx_e] = len(itr_e) - 1 # should be constant and minus one important

        return depths

    def next(self) -> bool:

        """ Computes the next entropies.

        :return: True if the next entropies have been computed, False otherwise.

        Notes:
        ------
        1. The next entropies are computed using the Gamma-Iteration."""
        
        # Accessing class attributes
        
        cs = self._cs
        
        es = self._es

        # Determine length of current entropies

        es_len = es.shape[0]
        
        if es_len == 1:
            
            # The scheme is already complete
            
            return False

        # Determine current entropy entry (unique)

        ee = np.where(self._cs == es_len)[0][0] + 1

        # Storing current entropy entry

        self._ee = ee
        
        # Built cumulative sum mask (avoid if-clause)
        
        cs_mask = np.array([0], dtype=INT_DTYPE)
        
        cs_mask = np.append(cs_mask, cs)
        
        # Apply Gamma-Iteration
        
        selection = [
            es[cs_mask[idx_e] : cs_mask[idx_e + 1]] for idx_e in range(ee)]
        
        next_es = np.empty((ee, len(es[0]) + 1), dtype=INT_DTYPE) # compute column size different ?

        for idx_s, itr_s in enumerate(selection):
                
                next_es[idx_s] = np.append(np.sum(itr_s.T[0]), itr_s[0])

        # List unpacks are not allowed in numba and np.array can not be applied to a list of ndarrays !

        # next_es = [
        #     np.append(np.sum(itr_s.T[0]), itr_s[0]) for itr_s in selection]
        
        # Store next entropies in entropies
        
        self._es = next_es
        
        # Successfully computed the next entropies
        
        return True
    

if __name__ == '__main__':

    pass