import numpy as np

from numba import int64, boolean
from numba.experimental import jitclass
from nexpy.global_settings import INT_DTYPE, ARRAY

""" deep_compatible_intger_list.py module

Notes:
------
- TODO Doc"""

spec = [
    ('_cs', int64[:]),
    ('_cs_2d_mask', int64[:, :]),
    ('_len', int64),
    ('_depth', int64),
    ('_indices', int64[:]),
    ('_element', int64),
    ('_element_history', int64[:, :]),
    ('_carry_history', int64[:]),
    ('_index', int64),
    ('_precomp', boolean)
]

@jitclass(spec)
class DeepCompatibleIntegerList(object):

    """ The DeepCompatibleIntegerList jitclass.

    Notes:
    ------
    - This class shall be used for integer lists which are deeply compatible.
    - The notion origins from the compatiblity with the specific design of the lower triangular factorised block matrix vector product: the deep compatible mvp.
    - There are leader and follower classes, which are used in tandem to generate the sequence of indices for the deep compatible mvp.
    - I opted to consolidate the follower and leader functionalities within the same class since inheritance with jitclasses (with the jitclass decorator) is currently not supported.
       In traditional object-oriented languages where JIT compilation is likely not utilized, one might typically contemplate inheriting a leader class from a follower class or a similar approach.
    - The existing implementation stores the generated sequence of indices, making it significantly more efficient when iterating through the sequence repeatedly.
      However, the matrix-vector product, which is the primary purpose of this class and operates in fast mode, does not utilize this feature. 
       -> It might be worth considering a redesign of the algorithm in order to use this feature.
    - The _comp_next method employs a mole strategy to sample the next indices.
      It may exceed the limit or not, but it cannot deterministically predict this beforehand.
      Instead, it operates blindly, much like a mole, simply executing the action.
    - TODO specify setter methods for the properties."""
    
    def __init__(self):
       
       """Initialises the DeepCompatibleIntegerList object.
       
       Notes:
       -----
       The init method is outsourced due to technical reasons."""
        
       pass

    def init(self, tiling: ARRAY, depth: INT_DTYPE):

        """Initialises the deep compatible integer list class.
        
        Args:
            tiling (ARRAY): The tiling.
            depth (INT_DTYPE): The depth.
        
        Notes:
        -----
        - The tiling is a 1D numpy array of integers.
        - The depth is an integer."""

        # Precompute length
        self._len = INT_DTYPE(len(tiling))

        # Initialise cumulative sum
        self._cs = np.cumsum(tiling) 
        
        # Initialise cumulative sum 2D mask
        cs_enum = np.append(np.array([0], dtype=INT_DTYPE), self._cs[: -1])
        
        self._cs_2d_mask = np.array([
            [itr_p, self._cs[idx_p]] 
            for idx_p, itr_p in enumerate(cs_enum)], dtype = INT_DTYPE)    
        
        # Determine depth
        self._depth = depth

        # Initialise carry history
        self._carry_history = -np.ones((self._len - 1), dtype = INT_DTYPE)

        # Set precomputed property
        self._precomp = False

        # Clear instance
        self.clear()
         
    def clear(self):

        """Clears the instance."""

        # Accessing class attributes
        depth = self._depth
        cs = self._cs
        length = self._len

        # Initialise global index
        self._index = 0

        # Initialise indices
        self._indices = np.zeros((max([depth - 1, 1])), dtype = INT_DTYPE)
        
        # Initialise element
        self._element = cs[0]

        # Initialise element history
        el_enum = np.empty((depth - 1, 2), dtype = INT_DTYPE)

        for idx_d in range(depth - 1):
                
                el_enum[idx_d] = [0, self._element]

        # List unpacks are not allowed in numba !
        self._element_history = np.concatenate((
             np.array([[0, length]], dtype = INT_DTYPE), el_enum), axis = 0)
        
    @property
    def cumsum(self) -> ARRAY:

        """Property method for the cumulative sum.

        Returns:
            ARRAY: The cumulative sum."""
        
        return self._cs
    
    @property
    def cumsum_2d_mask(self) -> ARRAY:

        """Property method for the two dimensional cumulative sum mask.
        
        Returns:
            ARRAY: The two dimensional cumulative sum mask."""
        
        return self._cs_2d_mask
    
    @property
    def length(self) -> INT_DTYPE:

        """Property method for the length.
        
        Returns:
            INT_DTYPE: The length."""
        
        return self._len
    
    @property
    def depth(self) -> INT_DTYPE:

        """Property method for the depth.
        
        Returns:
            INT_DTYPE: The depth."""
        
        return self._depth
    
    @property
    def indices(self) -> ARRAY:

        """Property method for the indices.
        
        Returns:
            ARRAY: The indices."""
        
        return self._indices
    
    @property
    def element(self) -> INT_DTYPE:

        """Property method for the element.
        
        Returns:
            INT_DTYPE: The element."""
        
        return self._element
    
    @property
    def element_history(self) -> ARRAY:

        """Property method for the element history.
        
        Returns:
            ARRAY: The element history."""
        
        return self._element_history

    @property
    def carry_history(self) -> ARRAY:

        """Property method for the carry history.
        
        Returns:
            ARRAY: The carry history."""
        
        return self._carry_history
     
    @property
    def index(self) -> INT_DTYPE:

        """Property method for the index.
        
        Returns:
            INT_DTYPE: The index."""

        return self._index
    
    @property
    def precomputed(self) -> bool:

        """Property method for the precomputed property.
        
        Returns:
            bool: The precomputed property."""
        
        return self._precomp
    
    def eval(self, indices: ARRAY) -> bool:

        """Evaluates the deep compatible integer list at the given indices.
        
        Args:
            indices (ARRAY): The indices.
            
        Returns:
            bool: True if the evaluation was successful, False otherwise.
            
        Notes:
        -----
        - NOTE Follower's method."""
        
        # Accessing class attributes
        depth = self._depth
        cs_2d_mask = self._cs_2d_mask
        length = self._len
        element_history = self._element_history
        
        # Copy indices to next_indices 
        last_indices = np.copy(self._indices)
        
        # Compute no-matches
        no_matches = np.where(np.not_equal(last_indices, indices))[0] # NOTE: optimize ...

        if len(no_matches) == 0:
            return True
        
        # Carry is the index of the first no-match
        j = no_matches[0]

        # Initialise kitkat
        kitkat = False
        
        # Check if carry already exceeds the prescribed depth
        if j >= depth:
            return False
        
        # Initialise element

        element = element_history[j]

        for i in range(j, depth - 1):
            idx = indices[i]

            # Compute next element
            if element[1] <= length:
                if idx < element[1] - element[0]:
                    element = cs_2d_mask[element[0] : element[1]][idx]
                    
                    if not i == depth - 1:
                        element_history[i + 1] = np.copy(element)

                else:
                    # Out of range
                    kitkat = True

                    break

            else:

                # Already completed iteration
                return False
            
        self._indices = np.copy(indices)
        self._element_history = element_history

        if not kitkat:
            self._element = element[1] - element[0]

            return True

        else:

            return False

    def next(self) -> bool:

        """ The wrapper for computing the next indices.
        
        Returns:
            bool: True if the next indices were computed, False otherwise.
        
        Notes:
        -----
        - NOTE Leader's method."""

        if self._len == 1:

            return False

        elif self._precomp:

            # Accessing class attributes
            cs = self._cs
            length = self._len
            indices = np.copy(self._indices)
            carry_history = self._carry_history
            index = self._index + 1

            # Check if exceeding

            if index == length:
                return False

            # Access current carry
            j = carry_history[index - 1]

            # Compute next indices
            indices[j + 1 :] = 0
            indices[j] += 1

            # Pass to properties
            self._element = cs[index] - cs[index - 1]
            self._indices = indices

            # Increase index by one
            self._index = index

            return True

        else:

            return self._comp_next()

    def _comp_next(self) -> bool:

        """ Computes the next indices.
        
        Returns:
            bool: True if the next indices were computed, False otherwise.
            
        Notes:
        -----
        - NOTE Leader's method."""
        
        # Accessing class attributes
        depth = self._depth
        indices = np.copy(self._indices)
        cs_2d_mask = self._cs_2d_mask
        length = self._len
        element_history = np.copy(self._element_history)
        index = self._index

        # Initialise carry
        j = depth - 2
        
        while True:
        
            indices[j] += 1
            
            # Initialise kitkat
            kitkat = False
        
            # Initialise element
            element = element_history[j]

            for i in range(j, depth - 1):

                idx = indices[i]

                # Compute next element
                if element[1] <= length:
                    if idx < element[1] - element[0]:
                        element = cs_2d_mask[element[0] : element[1]][idx]
                        
                        if not i == depth - 1:
                            element_history[i + 1] = np.copy(element)

                    else:
                        # Adjust carry
                        indices[j] = 0
                        j -= 1
                        kitkat = True

                        break

                else:
                    # Already completed iteration
                    self._precomp = True

                    return False
    
            if kitkat:
                continue
                
            else:
                self._indices = np.copy(indices)
                self._element = element[1] - element[0]
                self._element_history = np.copy(element_history)
                self._carry_history[index] = j
                self._index = index + 1

                return True


if __name__ == '__main__':
    
    pass