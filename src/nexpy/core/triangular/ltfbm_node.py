import numpy as np

from nexpy.core.triangular.rmo import RowMajorOrdering
from nexpy.global_settings import ARRAY, INT_DTYPE

"""ltfbm_node.py module"""

class LowerTriangularFactorisedBlockMatrixNode:

    """LowerTriangularFactorisedBlockMatrixNode class."""

    def __init__(self, first_piece: ARRAY, node: ARRAY):

        """Initiliases class.

        Args:
        -----
            first_piece: ARRAY
                The upper-left first piece of the original matrix.
            
            node: ARRAY
                The factors matrix or another first piece referring to a factorisation of mininum depth = 2 (programming depth not equal to actual theoretical depth.).

        Raises:
        -------
            ValueError: 
                If the first_piece has not the specified type or if it is a np.ndarray with dimensionality greater than 2.

        Notes
        -----
        - NOTE self._node is either leaf factors or another LTFBM Node
        - TODO Integrity checks"""

        # Integrity

        # TODO:: add either rmo or array for the following ...

        self._first_piece = RowMajorOrdering(first_piece)

        if isinstance(node, (np.ndarray, np.generic)):
            self._node = RowMajorOrdering(node)

        elif isinstance(node, RowMajorOrdering) | isinstance(node, LowerTriangularFactorisedBlockMatrixNode):
            self._node = node

        else:

            raise ValueError("Sorry, your input data for 'node' has not the right type.")

    @property
    def first_piece(self):
    
        """Property method for the first piece of the matrix."""

        return self._first_piece

    @property
    def is_leaf(self):

        """Property method for the leaf status of the node."""

        return isinstance(self._node, RowMajorOrdering)

    @property
    def leaf_factors(self):

        """Property method for the leaf factors of the node.
        
        Raises:
        -------
            Exception:
                If the instance is a leaf and has therefore no node."""

        if self.is_leaf:
            return self._node
        
        else:
            raise Exception("This instance is not a leaf and has therefore no leaf factors.")

    @property
    def node(self):

        """Property method for the node of the matrix.
        
        Raises:
        -------
            Exception:
                If the instance is a leaf and has therefore no node."""

        if not self.is_leaf:
            return self._node

        else:
            raise Exception("This instance is a leaf and has therefore no node.")

    @property
    def item(self, row: INT_DTYPE, col: INT_DTYPE, tiling: ARRAY):
        """Property method for the item of the matrix.
        
        Args:
        -----
            row: INT_DTYPE
                The row index of the item.
            
            col: INT_DTYPE
                The column index of the item.
            
            tiling: ARRAY
                The tiling of the matrix.
                
        Returns:
        --------
            FLOAT_DTYPE:
                The item of the matrix."""
        
        raise NotImplementedError("Sorry, this method is not implemented yet.")
    
    def factorise(self, max_depth: INT_DTYPE, positions: ARRAY, protocol: bool, total = 0):

        """Factorises the matrix.
        
        Args:
        -----
            max_depth: INT_DTYPE
                The maximal depth of the factorisation.
            
            positions: ARRAY
                The positions of the tiling.
            
            protocol: bool
                The protocol flag.
            
            total: INT_DTYPE
                The total amount of factorisations.

        Raises:
        -------
            Exception:
                If the leading entry is zero.

        Returns:
        --------
            bool, INT_DTYPE:
                The factorisation status and the total amount of factorisations.

        Notes:
        ------
        - NOTE:  Factorisation does not check if the matrix is factorisable from the entries POV."""
        
        # Reached the given maximal depth of the factorisation
        if total >= max_depth:
            return True, max_depth

        # The factorisation is applied to the node
        n = len(positions)

        index = np.where(positions == n)[0]

        if len(index) == 0:
            return True, total

        else:
            index = index[0]

        # Check if the matrix is already fully factorised
        if index == 0:
           return True, total

        positions = positions[0:index + 1]

        n = len(positions)

        leaf_entries_len = int(n*(n+1)/2)

        if not self.is_leaf:
            return False, None

        else:
            # Needs to be the zero-th entry of the tiling and is constant
            leaf_first_piece = self.leaf_factors.upper_sub_matrix(positions[0])

            # Check here for any entry being non zero is a better approach ...
            leading_entry = leaf_first_piece[0][0]

            if leading_entry == 0.0:

                exc_str = f'Sorry, the leading entry must be non-zero! \n' +\
                f'max depth = {max_depth}, positions = {positions}, current depth = {total} \n' +\
                f'leaf first peace =  {leaf_first_piece}.'

                raise Exception(exc_str)

            k = 0

            leaf_factors_entries = np.zeros((leaf_entries_len))

            mod_positions = [*[0], *positions[0:-1]] # Modification important!

            # Print protocol
            if protocol:

                print(f"""
                    --------------- TOTAL [{total}] ---------------\n
                    mod_positions:{mod_positions},\n
                    leaf_factors:\n{self.leaf_factors.reconstruct()},\n
                    item evaluations:\n""")

            # The following for loop could be optmisied in terms of time complexity
            for i in mod_positions:
                for j in mod_positions:
                    if j <= i:
                        if protocol:
                            print(f'[{i},{j}] = {self.leaf_factors.item(i, j)}\n')

                        leaf_factors_entries[k] = self.leaf_factors.item(i, j) / leading_entry
                        k += 1

            # Print protocol

            if protocol:
                print(f'\n')
                lf_c = RowMajorOrdering(leaf_factors_entries)
                print(f'leaf_factors_next:\n{lf_c.reconstruct()}\n\n')

            # Initiate node with a new child node
            self._node = LowerTriangularFactorisedBlockMatrixNode(
                first_piece = leaf_first_piece,
                node = leaf_factors_entries
            )

            return self._node.factorise(
                max_depth = max_depth,
                positions = positions,
                protocol = protocol,
                total = total + 1
            )
