import numpy as np

from numba import int64
from numba.experimental import jitclass

from .. import basic as basic
from .integer_list import IntegerList
from ...global_settings import ARRAY, INT_DTYPE

""" index.py module

Notes:
------
- TODO Doc"""


spec = [

    ('_value', int64),
    ('_history', int64[:]),
    
]

@jitclass(spec)
class Index(object):


    """Index jitclass."""

    def __init__(self, value: INT_DTYPE):

        """Constructor for index object.

        Args:
            value (INT_DTYPE): Value of the index.

        Returns:
            None: Nothing to return.
        """
        
        self._value = INT_DTYPE(value)

        self._history = np.empty(0, dtype = INT_DTYPE)

    def __str__(self) -> str:
        
        """String representation of the index object.

        Returns:
            str: String representation of the index object.
        """

        return self._to_string()

    # Properties
    
    @property
    def value(self) -> INT_DTYPE:

        """ Getter for the value of the index.
        
        Returns:
            INT_DTYPE: Value of the index.
        """

        return self._value

    @property
    def history(self) -> ARRAY:

        """ Getter for the history of the index.
        
        Returns:
            ARRAY: History of the index.
        """

        return self._history
    
    # Setter

    @value.setter
    def value(self, value: INT_DTYPE):

        """ Setter for the value of the index.

        Args:
            value (INT_DTYPE): Value to be set.

        Returns:
            None: Nothing to return.
        """

        self._value = INT_DTYPE(value)

    # History

    def _append_history(self, operation: str):

        """Appends the given operation to the history of the current index.

        Args:
            operation (str): Operation to be appended.

        Returns:
            None: Nothing to return.
        """

        history = self._history

        self._history = np.append(history, operation)

    def _copy_history(self, history: ARRAY):

        """Copies the given history to the history of the current index.

        Not recommended for performance reasons.

        Args:
            history (ARRAY): History to be copied.

        Returns:
            None: Nothing to return.
        
        """

        for itr_h in history:

            self._append_history(itr_h)

    def _to_symbol(self, history_index: INT_DTYPE) -> str:

        """ Converts the given history index to a string representation of the corresponding operation.

        Operations overview for the to_symbol method:
        
        #  | Operation
        ----------------------------
        0  | reduction

        Args:
            history_index (INT_DTYPE): History index to be converted.

        Returns:
            str: String representation of the corresponding operation.
        """

        if history_index == 0:

            return '\mathcal{R}'

    def _who_am_i(self) -> str:

        """ Converts the whole history of the index to a string representation of the index.

        Operations overview for the who_am_i method:

        #  | Operation
        ----------------------------
        0  | reduction
        1  | relative
        
        Returns:
            str: String representation of the index.
        """

        # Accessing class attributes

        history = self._history

        # Compute history length

        history_len = len(history)

        # Produce who_am_i string

        res = "I"

        i = 0

        brackets = 0

        if not history_len == 0:

            while i < history_len:

                if history[i] == 1:

                    res += brackets*'}'

                    brackets = 0

                    res = 'rel \left( ' + res + ", L\\right)"

                else:

                    res += '^{'

                    brackets += 1

                    amount = 0

                    while history[i + amount] == history[i]:

                        amount += 1

                        if i + amount == history_len:

                            break

                    sym = self._to_symbol(history[i])

                    res += sym + '^{' + str(amount) + '}'

                    i += amount-1

                i += 1

        return res + brackets*'}'

    def _to_string(self) -> str:

        """To string method for index object.

        Returns: 
            str: String representation of the index object.
        """

        # Accessing class attributes

        value = self._value

        # Produce string

        res = self._who_am_i() + " = " + value

        return res

    # Simple

    def plusplus(self):
            
            """Increments the value of the index by one.
    
            Returns:
                None: Nothing to return.
            """
    
            self._value += 1

    # Complex

    def reduction(self, L: IntegerList):
        
        """Computes the reduction of the current index with respect to the given integer list.

        :param L: IntegerList object.
        :return: Another index object that is the reduction of the current index with respect to the given integer list.

        Notes:
        ------
        Return type can not be specified as Index due to the limitations of the numba jitclass."""

        # Accessing class attributes
        
        value = self._value

        history = self._history

        # Index reduction

        reduction = Index(0)

        cs = L.cumsum().entries

        for itr_c in cs:

            if value < itr_c:

                break

            else:

                reduction.plusplus()

        # Copy existing history

        reduction._copy_history(history)

        # Add the reduction operation to the history

        reduction._append_history(0)

        # Return index reduct

        return reduction

    def relative(self, L: IntegerList):

        """Computes the relative index of the current index with respect to the given integer list.

        Returns:
            Index: Another index object that is the relative index of the current index with respect to the given integer list.
        """

        # Accessing class attributes

        value = self._value

        # Compute relative index

        rel_index = Index(value)

        for itr_l in L.entries:

            reduce = rel_index.value - itr_l

            if reduce >= 0:

                rel_index.value = reduce

            else:

                break

        return rel_index

    def restoration(self, L: IntegerList, depth : INT_DTYPE) -> ARRAY:

        """Computes the restoration of the current index with respect to the given integer list.

        :param L: IntegerList object.
        :param depth: Depth of the restoration.
        :return: A list of index values which represent the restoration of the current index with respect to the given integer list."""

        index = self

        indices = np.zeros((depth), dtype = INT_DTYPE)

        for _ in range(depth):

            if _ < depth - 1:

                indices[_] = index.relative(L=L).value
                
                index = index.reduction(L=L)
            
            else:

                indices[_] = index.value

        return indices


def index_reduction_chain(index) -> basic.Chain:

        """ Instantiates the reduction chain of the given index.

        Returns:
            basic.Chain: Chain of corresponding reductions of the given index.
        """

        return basic.Chain(
            element = index,
            lambda_expr = lambda element : element.reduction())