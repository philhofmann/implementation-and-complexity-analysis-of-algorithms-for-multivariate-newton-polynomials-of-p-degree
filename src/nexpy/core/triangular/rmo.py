import sys
import numpy as np
import numba as nb

from numba import njit
from nexpy.global_settings import ARRAY, FLOAT_DTYPE, INT_DTYPE

""" This module contains the RowMajorOrdering class and its jit compiled utility functions."""

@njit
def jit_rmo_dot(entries, vec):

    """Computes the matrix vector product for row major ordering.

    Args:
    -----
        entries: ARRAY
            The entries of the row major ordering format.

        vector: ARRAY
            The vector which shall be multiplied.
    
    Returns:
    --------
        ARRAY: 
            The matrix vector product ``A @ x``, where ``A`` is the original matrix and ``x`` is the given vector.

    Notes:
    ------
    NOTE: the choice of n avoids size conflicts on both sides i.e. a vector which is to long or entries which are too many!"""

    n = min([len(vec), int((np.sqrt(1 + 8 * len(entries) - 1)) / 2)])

    res = np.zeros(n, dtype=np.float64)

    k = 0

    for i in range(n):

        k_next = k + i + 1

        res[i] = np.sum(entries[k:k_next]*vec[0:i + 1])

        k = k_next

    return res

@njit
def jit_rmo_reconstruct(entries):

    """Reconstruct the row major ordering format
    
    Args:
    -----
        entries: ARRAY
            The entries of the row major ordering format.
    
    Returns:
    --------
        ARRAY:
            The reconstructed original matrix ``A``."""

    L = len(entries)

    N = int(np.sqrt(1 + 8 * L)/2)

    res = np.zeros((N, N))

    i, j = 0, 0

    for k in range(L):

        res[i][j] = entries[k]

        if i == j:

            i += 1
            j = 0

        else:

            j+= 1

    return res

class RowMajorOrdering(object):

    def __init__(self, data: ARRAY):

        """Initiliase class.

        If the data has no quadratic shape, this initialisation method will cut it off.

        Args:
        -----
            data: ARRAY
                Either the data of the matrix (dim = 2) or the row majoring data (dim = 1).
        
        Raises:
        -------
            ValueError:
                If the data has dimensionality greater than 2."""

        dim = len(data.shape)

        if dim == 1:
            self._entries = data.astype(FLOAT_DTYPE)

        elif dim == 2:
            self._entries = RowMajorOrdering.extract_entries(matrix = data).astype(FLOAT_DTYPE)

        else:
            raise ValueError("Sorry, only one or two dimensional data is supported.")

    # Initialisation methods

    @staticmethod
    @njit
    def extract_entries(matrix: ARRAY):

        N = min(matrix.shape)

        size = INT_DTYPE(N * (N + 1) / 2)

        entries = np.zeros((size), dtype = FLOAT_DTYPE)

        n = 0

        for i in nb.prange(N):

            for j in nb.prange(i + 1):

                entries[n] = FLOAT_DTYPE(matrix[i][j])

                n += 1

        return entries

    # Stored properties

    @property
    def data(self):

        """Property method for data."""

        return [self._entries]

    @property
    def entries(self):

        """Property method for entries."""

        return self._entries

    # Computed properties

    @property
    def size(self):

        """Property method for the size of the original matrix.

        The formula is the positive solution of: n * (n + 1) / 2 - len(entries) = 0.
        At the same time, we assume that the matrix was originally quadratic."""

        if not self._entries is None:

            n = len(self._entries)

            return int(0.5 * (np.sqrt(8 * n + 1) - 1))

        else:

            return 0

    @property
    def bytes(self):

        """Property method for the storage size in bytes.

        It is given by the storage size of the entries."""

        return sys.getsizeof(self._entries)

    def item(self, row: INT_DTYPE, col: INT_DTYPE):

        """Gives back the item w.r.t. row, col.

        This method computes the position in the entries array by: index = col + row * (row + 1) / 2.

        Args:
        -----
            row: INT_DTYPE
                The row.

            col: INT_DTYPE
                The col.

        Raises:
        -------
            ValueError:
                If the row exceeds the size.

            ValueError:
                If the column exceeds the size.


        Returns:
        --------
            FLOAT_DTYPE:
                The desired entry.
        """

        # Accessing size

        size = self.size

        # Handling exceptions

        if row >= size:
            raise ValueError("Sorry, but the row exceeds the size of the matrix.")

        if col >= size:
            raise ValueError("Sorry, but the column exceeds the size of the matrix.")

        # Utilize that we are dealing with a lower triangular matrix

        if col > row:
            return 0.0

        else:
            index = int(col + row * (row + 1) / 2)

            return self._entries[index]

    # CLASS METHODS

    def upper_sub_matrix(self, n: INT_DTYPE):

        """Computes the upper left sub matrix of quadratic size n.

        This method is using a JIT compiled method: jit_rmo_reconstruct.
        
        Args:
        -----
            n: INT_DTYPE
                Desired amount of rows (= amount of rows) of the sub matrix.

        Returns:
        --------
            ARRAY:
                Upper sub matrix (left upper).
        
        Notes:
        ------
        - TODO : n = 0 results in [[0.0]]"""

        entries_amount = int(n * (n + 1) / 2)

        return jit_rmo_reconstruct(self._entries[0:entries_amount])

    def dot(self, vec: ARRAY) -> ARRAY:
        """Computes the matrix vector product.

        This method is using an external jit compiled method: jit_rmo_dot.

        Args:
        -----
            vec: ARRAY
                Vector with the right shape (see : size method).

        Raises:
        -------
            ValueError:
                If the vector has not the specified type or shape.
        
        Returns:
        --------
            ARRAY:
                Matrix-vector product (short : mvp)."""

        # Acess class attributes
        entries = self._entries

        # Access size
        size = self.size

        # Handle exception
        if len(vec) == size:

            # Compute the actual dot product
            return jit_rmo_dot(
                entries = entries,
                vec = vec.astype(FLOAT_DTYPE))

        else:

            raise ValueError("Sorry, but the length of the vector should coincide with the size of the matrix.")

    def reconstruct(self) -> ARRAY:
        """Reconstruct the matrix in its original form.

        This method is using an external jit compiled method: jit_rmo_reconstruct.

        Raises:
        -------
            ValueError:
                If the entries of the class are None.
            
        Returns:
        --------
            ARRAY:
                The original matrix ``A``."""

        if (not self._entries is None):

            return jit_rmo_reconstruct(self._entries)

        else:

            raise ValueError("Sorry, the entries are None and thus the reconstruction is not feasible.")

    