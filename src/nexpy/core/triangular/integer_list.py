import numpy as np
from nexpy.core.basic import Chain

from numba import int64
from numba.experimental import jitclass
from nexpy.global_settings import ARRAY, INT_DTYPE

""" integer_list.py

Notes
-----
- In the current implementation the depth of an integer list is the size of the entropy list minus 1.
  So the actual depth = implementation depth - 1.
  -> Wanted to avoid conflicts when changing the depth of an integer list. 
- TODO: Change the depth of an integer list"""


spec = [
    ('_entries', int64[:]),
    ('_history', int64[:]),
]

@jitclass(spec)
class IntegerList(object):

    """This is the IntegerList jitclass."""

    def __init__(self, entries: ARRAY):

        """Constructor for IntegerList object.

        Args:
        -----
            entries: ARRAY
                Entries of the integer list."""
        
        self._entries = entries.astype(INT_DTYPE)
        self._history = np.empty(0, dtype = INT_DTYPE)

    def __str__(self) -> str:
        
        """String representation of the integer list.

        Returns:
        --------
            str:
                String representation of the integer list."""

        return self._to_string()
    
    @property
    def entries(self) -> ARRAY:

        """Property method for the entries of the integer list."""

        return self._entries

    @property
    def history(self) -> ARRAY:

        """Property method for the history of the integer list."""

        return self._history

    @property
    def length(self) -> INT_DTYPE:

        """Property method for the length of the integer list."""

        return INT_DTYPE(self._entries.size)

    @property
    def size(self) -> INT_DTYPE:

        """Property method for the size of the integer list."""

        return INT_DTYPE(np.sum(self._entries))

    @property
    def is_non_trivial(self):
            
        """ Property method for the non trivial property. """

        return np.all(self._entries >= 1)

    @property
    def is_dominated(self):
        
        """Property method for the dominated property. """

        return np.all(self._entries <= self._entries[0])

    @property
    def is_monotonous(self):
            
        """Property method for the monotonous property. """

        return np.all(np.diff(np.append(self._entries, 0)) <= 0)

    # History

    def _append_history(self, applied: str):

        """Appends the applied operation to the history of the integer list.

        Args:
        -----
            applied: str
                The applied operation."""

        history = self._history

        self._history = np.append(history, applied)

    def _copy_history(self, history: ARRAY):

        """Copies the history of the given integer list.

        Args:
        -----
            history: ARRAY
                The history of an integer list."""

        for itr_h in history:

            self._append_history(itr_h)

    def _to_symbol(self, history_index: INT_DTYPE) -> str:

        """Converts the given history index to a string representation of the corresponding operation.

        Operations overview for the _to_symbol method:
        
            #  | Operation
            --------------
            0  | reduction
            1  | entropy

        Args:
        -----
            history_index: INT_DTYPE
                History index to be converted.

        Returns:
        --------
            str:
                String representation of the corresponding operation."""

        if history_index == 0:

            return '\mathcal{R}'

        elif history_index == 1:

            return '\mathcal{E}'

    def _who_am_i(self) -> str:

        """Converts the whole history of the integer list to a string representation.
        
        Operations overview for the _who_am_i method:
        
            #  | Operation
            --------------
            0  | reduction
            1  | entropy
            3  | cumulative sum
            4  | differences
            5  | apply
            6  | sub
        
        Returns:
        --------
            str:
                String representation of the history of the integer list."""

        # Accessing class attributes
        history = self._history

        # Compute history length
        history_len = len(history)

        # Produce _who_am_i string
        res = 'L'

        i = 0

        brackets = 0

        if not history_len == 0:

            while i < history_len:

                if history[i] == 3:

                    res += brackets*'}'

                    brackets = 0

                    res = '\overline{' + res + '}'

                elif history[i] == 4:

                    res += brackets*'}'

                    brackets = 0

                    res = '\left(\Delta ' + res + "\\right)"

                elif history[i] == 5:

                    res += brackets*'}'

                    brackets = 0

                    res = '\left(' + res + '.apply' + "\\right)"

                elif history[i] == 6:

                    res += brackets*'}'

                    brackets = 0

                    res = '\left(' + res + '.sub' + "\\right)"

                else:

                    res += '^{'

                    brackets += 1

                    amount = 0

                    while history[i + amount] == history[i]:

                        amount += 1

                        if i + amount == history_len:

                            break

                    sym = self._to_symbol(history[i])

                    res += sym + '^{' + str(amount) + '}'

                    i += amount-1

                i += 1

        return res + brackets*'}'

    def _to_string(self) -> str:

        """To string method for the integer list.

        Returns:
        --------
            str: String representation of the integer list."""

        # Accessing class attributes
        entries = self._entries

        # Produce string
        res = ' = ('

        for entry in entries:

            if not len(res) == 4:

                res += ', '

            res += str(entry)

        return self._who_am_i() + res + ')'

    # Simple
    
    def item(self, position : INT_DTYPE) -> INT_DTYPE:
        """Returns the item at the given position.

        Args:
        -----
            position: INT_DTYPE
                Position of the item to be returned.

        Returns:
        --------
            INT_DTYPE: 
                Item at the given position."""

        return self._entries[position]

    def where(self, number: INT_DTYPE) -> INT_DTYPE:

        """Returns the first index where an entry equals the given number.

        Args:
        -----
            number: INT_DTYPE
                Number to be searched for.
        
        Returns:
        --------
            INT_DTYPE:
                First index where an entry equals the given number."""

        # Accessing class attributes
        entries = self._entries

        # Get the first index where an entry equals the given number
        index = np.where(entries == number)[0]

        if len(index) == 0:
            return None

        else:
            return index[0]

    # Basic

    def flip_order(self):

        """Flips the order of the entries of the integer list.

        Returns:
        --------
            IntegerList:
                Integer list with flipped order.

        Notes:
        ------
        - TODO: Introduce history symbol for this operation"""

        # Accessing class attributes
        entries = self._entries
        history = self._history

        # Flip the order
        f_o = entries[::-1]

        # Instantiate IntegerList
        flip_order = IntegerList(f_o)

        # Copy existing history
        flip_order._copy_history(history)

        # TODO: Add flip order operation to history
        # flip_order._append_history(# some number)

        return flip_order

    def flip_sign(self):

        """Flips the sign of the entries of the integer list.
        
        Returns:
        --------
            IntegerList:
                Integer list with flipped sign.

        Notes:
        ------
        - TODO : Introduce history symbol for this operation."""

        # Accessing class attributes
        entries = self._entries
        history = self._history

        # Flip the sign
        f_s = -entries

        # Instantiate IntegerList
        flip_sign = IntegerList(f_s)

        # Copy existing history
        flip_sign._copy_history(history)

        # TODO: Add flip sign operation to history
        # flip_sign._append_history(# some number)

        return flip_sign

    def differences(self):
        
        """Computes the differences of the entries of the integer list.
        
        Returns:
        --------
            IntegerList:
                Integer list of the differences of the entries.
            
        Notes:
        ------
        - TODO: Introduce history symbol for this operation."""

        # Accessing class attributes

        entries = self._entries
        history = self._history

        # Compute differences
        diffs = np.diff(np.append(entries, 0))

        # Instantiate IntegerList
        differences = IntegerList(diffs)

        # Copy exisiting history
        differences._copy_history(history)

        # Add differences operation to history
        differences._append_history(4)

        return differences

    def cumsum(self):

        """Computes the cumulative sum of the entries of the integer list.
        
        Returns:
        --------
            IntegerList:
                Integer list of the cumulative sum of the entries."""

        # Accessing class attributes
        entries = self._entries
        history = self._history

        # Computing cumulative sum
        cs = np.cumsum(entries)

        # Instantiate IntegerList
        cumsum = IntegerList(cs)

        # Copy existing history
        cumsum._copy_history(history)

        # Add cumulative sum to history
        cumsum._append_history(3)

        return cumsum

    def sub(self, start: INT_DTYPE, end: INT_DTYPE):

        """Returns an integer list with the entries cropped from start to end.
        
        Args:
        -----
            start: INT_DTYPE
                Start index of the sub entries.

            end: INT_DTYPE
                End index of the sub entries.

        Returns:
        --------
            IntegerList:
                Integer list with the entries cropped from start to end.

        Notes:
        ------
        - TODO Poor history handling."""

        # Accessing class attributes
        entries = self._entries
        history = self._history

        # Create sub entries
        sub_entries = entries[start : end]

        # Instantiate IntegerList
        sub = IntegerList(sub_entries)

        # Copy existing history
        sub._copy_history(history)

        # Add sub to history
        sub._append_history(6)

        return sub

    # Complex

    def apply(self, il):
            
        """Applies the given integer list to the integer list.

        Args:
        -----
            il:
                Given integer list which will be applied.

        Returns:
        --------
            IntegerList:
                Resulting integer list.

        Notes:
        ------
        - TODO Poor history handling."
        - TODO Check if size of il is not greater than self.size"""

        # Accessing class attributes
        history = self._history

        # Apply the given integer list
        length = il.length
        cs = il.cumsum()
        appl = np.array([self.sub(0 if idx_l == 0 else cs.item(idx_l - 1), cs.item(idx_l)).size for idx_l in range(length)], dtype = INT_DTYPE)

        # Instantiate IntegerList
        applied = IntegerList(appl)

        # Copy existing history
        applied._copy_history(history)

        # Add applied to history
        applied._append_history(5)

        return applied

    def entropy(self):

        """Computes the entropy of the integer list.
        
        Returns:
        --------
            IntegerList:
                Entropy of the integer list"""

        # Accessing class attributes
        history = self._history

        # Compute cumulative sum
        cs = self.cumsum().entries

        # Compute entropy
        e = np.array([cs[-1]], dtype = INT_DTYPE)

        if not e[0] == 1:

            while True:

                l = e[-1]

                temp = cs[0: l]

                index = np.where(temp == l)[0]

                if len(index) == 0:

                    break

                else:

                    index = index[0]

                e = np.append(e, index + 1)


        # Instantiate an IntegerList
        entropy = IntegerList(e)

        # Copy existing history
        entropy._copy_history(history)

        # Add the entropy operation to the history
        entropy._append_history(1)

        # Return entropy
        return entropy

    def depth(self) -> INT_DTYPE:

        """Computes the depth of the integer list."""

        return self.entropy().length - 1

    def reduction(self):

        """Computes the reduction of the integer list.
        
        Returns:
        --------
            IntegerList:
                Reduction of the integer list."""

        # Accessing class attributes
        length = self.length
        entries = self._entries
        history = self._history

        # Compute cumulative sum
        cumsum = self.cumsum()

        # Find (first) index such that entries exactly sum up to length
        index = cumsum.where(length)

        if index is None:
            return None

        else:
            # List reduction
            reduction = IntegerList(entries[0 : index + 1])

            # Copy existing history
            reduction._copy_history(history)

            # Add the reduction operation to the history
            reduction._append_history(0)

            # Return list reduct
            return reduction

    def is_compatible(self, depth=None, debug=False) -> bool:

        """Checks whether the integer list is compatible.

        Returns:
        --------
            bool:
                True if the integer list is compatible, False otherwise."""

        # Accessing class attributes
        if depth is None:
            depth = self.depth()

        if depth is not None: # else statement not accepted by numba
            if depth > self.depth() or depth < 2:
                print(f"Given depth is not valid.")

                return False
        
        # Check whether is dominated and non trivial
        if not self.is_dominated and not self.is_non_trivial:
            if debug:
                print(f"This integer list is not dominated and non trivial")

            return False
        
        if not self.is_dominated:
            if debug:
                print(f"This integer list is not dominated")

            return False
        
        if not self.is_non_trivial:
            if debug:
                print(f"This integer list is trivial")

            return False

        # Compute the (depth - 2)-th reduction
        il = self

        for _ in range(depth - 2): # 
            il = il.reduction()

        # Check if monotonously non-increasing from bottom to top 
        for _ in range(depth - 1):
            if not il.is_monotonous:

                return False

            if not _ == depth - 2:
                # Apply il to self
                il = self.apply(il)
        
        return True

    def is_deep_compatible(self) -> bool:

        """Checks whether the integer list is deep compatible.

        Returns:
        --------
            bool:
                True if the integer list is deep compatible, False otherwise."""

        return self.is_compatible() & self.entropy().item(-1) == 1

def integer_list_reduction_chain(il: IntegerList) -> Chain:

    """Instantiates a chain of the reductions of the given integer list.

    Args:
    -----
        il: IntegerList
            The integer list for which the reduction chain will be created.

    Returns:
    --------
        Chain: 
            Chain of the reductions of the given integer list."""

    return Chain(
        element = il,
        lambda_expr = lambda element : element.reduction())

if __name__ == '__main__':

    pass


