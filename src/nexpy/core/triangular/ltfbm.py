import sys
import numba
import warnings
import numpy as np

from numba import njit
from typing import Tuple
from nexpy.core.triangular.index import Index
from nexpy.core.triangular.rmo import RowMajorOrdering
from nexpy.core.triangular.integer_list import IntegerList
from nexpy.core.triangular.entropy_scheme import EntropyScheme
from nexpy.core.triangular.rmo import jit_rmo_dot, jit_rmo_reconstruct
from nexpy.jit_compiled_utils import jit_concatenate_2d
from nexpy.core.triangular.ltfbm_node import LowerTriangularFactorisedBlockMatrixNode
from nexpy.global_settings import ARRAY, FLOAT_DTYPE, INT_DTYPE
from nexpy.core.triangular.deep_compatible_integer_list import DeepCompatibleIntegerList

""" This module contains the LowerTriangularFactorisedBlockMatrix class and its jit compiled utility functions. """

@njit
def jit_ltfbm_item(row, col, tiling, chain): # CHANGED: CHECK !

    """Restores a single item of the original matrix.

    Args:
    -----
        row: INT_DTYPE
            Row index of the item.
        
        col: INT_DTYPE
            Column index of the item.
        
        tiling:
            Tiling of the matrix.
        
        chain:
            Chain of the ltfbm.

    Returns:
    --------
        FLOAT_DTYPE:
            The restored item."""

    if col > row:

        return 0.0

    # Compute restoration indices

    item = 1.0

    depth = INT_DTYPE(len(chain))
    
    row_index, col_index = Index(row), Index(col)

    restoration = np.stack((
        row_index.restoration(tiling, depth),
        col_index.restoration(tiling, depth)), axis=1)

    for idx in range(depth):

        i, j = restoration[idx]

        # Use lower triangular matrix property

        if j > i:

            return 0.0

        # Compute index for row major ordering
        idx_rmo = (int)(j + i * (i + 1) / 2)

        # Store factor
        factor = chain[idx][idx_rmo]

        if factor == 0.0:
            return 0.0

        else:
            # Multiply past result by the right factor
            item *= factor

    return item

@njit
def jit_ltfbm_dot_deep_compatible(tree : ARRAY, til: ARRAY, vec : ARRAY, e_scheme : EntropyScheme, dcf : DeepCompatibleIntegerList, dcl : DeepCompatibleIntegerList):

    """Compute the Deep Compatible Matrix-Vector Product
    
    Args:
    -----
        tree: ARRAY
            L-Factorisation chain.
        
        til: ARRAY
            Tiling of the original matrix.
        
        vec: ARRAY
            Vector which should be multiplicated.
        
        e_scheme: EntropyScheme
            Empty entropy scheme - technical reason.

        dcf: DeepCompatibleIntegerList
            Empty deep compatible integer list - technical reason - ``Deep Compatible Follower``.
        
        dcl: DeepCompatibleIntegerList
            Empty deep compatible integer list - technical reason - ``Deep Compatible Leader``.

    Returns:
    --------
        ARRAY:
            The result of the matrix-vector product ``A @ x``, where ``A`` denotes the original matrix and x denotes the vector."""

    #---------------------------------------

    # -> Precompute e_scheme and dcs

    # e_scheme = EntropyScheme() # entropy scheme

    # dcf = DeepCompatibleIntegerList() # deep compatible follower

    # dcl = DeepCompatibleIntegerList() # deep compatible leader

    # Compute size

    size = np.sum(til)

    # Result vector initialisation

    res = np.zeros((size), dtype = FLOAT_DTYPE)

    #--------------------------------------

    # Initialise entropy scheme

    e_scheme.init(til) # tiling
    
    # Utilize entropy scheme for looping

    i = 0

    last_pr_0 = None

    last_pr_1 = None

    last_dp = None

    while e_scheme.next():

        # Sub result vector initialisation

        sub_res = np.zeros((size), dtype = FLOAT_DTYPE)

        # Store current entropy

        e = e_scheme.entropy_entry

        # Store first piece

        fp = tree[i]

        # Get first coordinate projection of entropy scheme (flat tiling)

        pr_0 = e_scheme.projection(0)

        # Get second coordinate projection

        pr_1 = e_scheme.projection(1)

        # Get depth projection regarding entropies

        dp = e_scheme.depth_projection()

        # Compute next result

        if not i == 0:

            # Initialise index and positions

            pos_batch = 0

            pos_chunk = 0

            pos_til = 0

            pos_chunk_f = 0 # position chunk follower

            for idx in range(e):

                # Compute next batch position 

                next_pos_batch = pos_batch + pr_0[idx]

                # Compute next chunk position

                next_pos_chunk = pos_chunk + pr_1[idx]

                # Compute next tiling position

                next_pos_til = pos_til + til[idx]

                # Determine chunk

                chunk = til[pos_chunk : next_pos_chunk]

                # Initialise chunk tiling

                chunk_til = last_pr_1[pos_til : next_pos_til]

                # Initialise total count

                n = 0

                # Initialise positions

                pos_chunk_til_f = 0 # position chunk tiling follower

                for idx_f, itr_f in enumerate(chunk_til):

                    next_pos_chunk_f = pos_chunk_f + last_pr_0[pos_til + idx_f] 

                    next_pos_chunk_til_f = pos_chunk_til_f + itr_f

                    follower = chunk[pos_chunk_til_f : next_pos_chunk_til_f]

                    dcf.init(
                        follower, # tiling
                        INT_DTYPE(last_dp[pos_til + idx_f])) # depth
                    
                    pos_chunk_til_l = 0 # position chunk tiling leader

                    pos_sub_vec_l = pos_batch # position sub vector leader

                    for idx_l, itr_l in enumerate(chunk_til[: idx_f + 1]):

                        next_pos_chunk_til_l = pos_chunk_til_l + itr_l

                        leader = chunk[pos_chunk_til_l : next_pos_chunk_til_l]

                        dcl.init(
                            leader, # tiling
                            INT_DTYPE(last_dp[pos_til + idx_l])) # depth
                        
                        # Initialise sub_vec
                    
                        sub_vec = np.zeros((last_pr_0[pos_til + idx_f]), dtype = FLOAT_DTYPE)

                        # Assign sub_vec values

                        pos_sub_vec_f = 0 # position sub vector follower

                        dcl_next = True # deep compatible leader next

                        while dcl_next: 

                            indices = dcl.indices

                            next_pos_sub_vec_l = pos_sub_vec_l + dcl.element

                            if dcf.eval(indices):

                                next_pos_sub_vec_f = pos_sub_vec_f + dcf.element

                                sub_vec[pos_sub_vec_f : next_pos_sub_vec_f] = res[pos_sub_vec_l : pos_sub_vec_l + dcf.element]

                                pos_sub_vec_f = next_pos_sub_vec_f
                                
                            else:
                                
                                pass # Do nothing

                            # Increments

                            pos_sub_vec_l = next_pos_sub_vec_l

                            dcl_next = dcl.next()
                          
                        # Assign sub result

                        sub_res[pos_chunk_f : next_pos_chunk_f] += np.copy(fp[n] * sub_vec) # NOTE: idx_s right?

                        # Increments

                        n += 1

                        pos_chunk_til_l = next_pos_chunk_til_l

                    # Increments
                        
                    pos_chunk_f = next_pos_chunk_f
                    
                    pos_chunk_til_f = next_pos_chunk_til_f
                
                # Increments

                pos_batch = next_pos_batch

                pos_chunk = next_pos_chunk

                pos_til = next_pos_til

        else:

            # Initialise position

            pos = 0

            for id_f in range(e):

                next_pos = pos + pr_0[id_f]
                
                sub_res[pos : next_pos] = jit_rmo_dot(
                        entries = fp,
                        vec = vec[pos : next_pos])
                
                pos = next_pos

        # Increase i by one

        i += 1

        last_pr_0 = np.copy(pr_0)

        last_pr_1 = np.copy(pr_1)

        last_dp = np.copy(dp)

        res = np.copy(sub_res)

    return res

@njit
def jit_ltfbm_dot_compatible(tiling, tree, entropy, vec):

    """Compute the Compatible Matrix-Vector Product
    
    Args:
    -----
        til: ARRAY
            Tiling of the original matrix.

        tree: ARRAY
            L-Factorisation chain.
        
        entropy: ARRAY
            Entropy of the tiling L.
        
        vec: ARRAY
            Vector which should be multiplicated.

    Returns:
    --------
        ARRAY:
            The result of the matrix-vector product A @ x, where A denotes the original matrix and x denotes the vector.
            
    Notes:
    ------
    - NOTE This algorithm uses deep single value restoration, if the leaf factors matrix itself is factorized."""

    size = tiling.size
    res = np.zeros(size, np.float64)
    helper_vec = np.zeros(size, np.float64)
    first_piece_entries = tree[0]

    # Determine tiling and tree of the factor matrix (by construction)
    fac_tiling = tiling.reduction()
    fac_chain = tree[1 :]
    fac_depth = len(fac_chain)

    # We are not going to need the first entry of the entropy 
    entropy.pop()

    # Compute matrix vector product with deep single value restoration
    n, pos = 0, 0

    for i, slot in enumerate(tiling.entries):

        # Precompute and store next position
        next_pos = pos + slot

        # Compute first level matrix-vector products
        helper_vec[pos : next_pos] = jit_rmo_dot(
            first_piece_entries,
            vec[pos : next_pos])

        inner_pos = 0
    
        for j in range(i+1):

            # Compute leaf factor
            leaf_factor = 0.0

            if fac_depth > 1:
                leaf_factor = jit_ltfbm_item(
                    row = i,
                    col = j,
                    tiling = fac_tiling,
                    chain = fac_chain)

            else:
                leaf_factor = fac_chain[0][n]

            res[pos:next_pos] += leaf_factor * helper_vec[inner_pos : inner_pos + slot]
            inner_pos += tiling.entries[j]
            n += 1

        pos = next_pos

    return res

@njit
def jit_ltfbm_reconstruct(first_piece_entries, leaf_factors_entries, tiling):

    """Reconstructs the whole matrix from the first piece and the leaf factors.

    Args:
    -----
        first_piece_entries: ARRAY
            The entries of the first piece - row major ordering format!
        
        leaf_factors_entries: ARRAY
            The entries of the leaf factors - row major ordering format!
        
        tiling: ARRAY
            The tiling of the original matrix ``A``.

    Returns:
    --------
        ARRAY:
            The original matrix ``A``."""

    #size = np.sum(tiling)

    blocks = list()#np.empty((size, size), dtype = FLOAT_DTYPE)

    l = len(tiling)

    n = 0

    fp_rec = jit_rmo_reconstruct(first_piece_entries)

    for i, slot in enumerate(tiling):

        # Reconstruct row-major ordered first_piece_entries

        block =  fp_rec[0 : slot].T[0 : slot].T

        block_list = [

            jit_concatenate_2d(

                    a = (leaf_factors_entries[n + j] * block).T,
                    b = np.zeros((tiling[j] - slot, slot), dtype = np.float64)

                    ).T if tiling[j] - slot >= 0
            
            else (leaf_factors_entries[n + j] * block)[:, :tiling[j]]

            for j in range(i + 1)]
        
        pad_list = [

            np.zeros((slot, tiling[j]), dtype = np.float64)

            for j in range(i+1, l)
        ]

        block_list.extend(pad_list)

        blocks.append(block_list)

        n += i + 1

    return blocks

class LowerTriangularFactorisedBlockMatrix:

    def __init__(self, first_piece: ARRAY, node: ARRAY, tiling: ARRAY):

        """Initiliase class.

        Args:
        -----
            first_piece: ARRAY
                The upper-left first piece of the original matrix.

            node: ARRAY
                The factors matrix or another first piece referring to a factorisation of mininum depth = 2 (CAUTION: Programming depth = actual depth + 1)

            tiling: ARRAY
                The tiling of the original matrix. For instance for a 7x7 matrix, tiling = [3,2,2,1] is a valid choice.
                In this case the first_piece need to have shape 3x3 (first entry of the tiling) and the node,
                if it is given by a leaf_factors matrix needs to have shape 4x4 (length of the tiling).

        Raises:
        -------
            ValueError:
                If the first_piece has not the specified type or if it is a np.ndarray with dimensionality greater than 2.
            
            ValueError:
                If the node has not the specified type or if it a np.ndarray with dimensionality greater than 2.

            ValueError:
                If the tiling has not the specified type or if the tiling does not match with the first_piece and the node."""

        # Integrity checks are located also in LowerTriangularFactorisedBlockMatrixNode
        self._root = LowerTriangularFactorisedBlockMatrixNode(
            first_piece = first_piece,
            node = node)

        # Integrity checks for tiling and set tiling
        if isinstance(tiling, IntegerList):
            self._tiling = tiling

        elif isinstance(tiling, ARRAY):
            self._tiling = IntegerList(tiling)

        else:
            raise ValueError("Sorry, your input data for 'tiling' has not the correct type.")

        if not first_piece.shape[0] == first_piece.shape[1] or not self._tiling.item(0) == first_piece.shape[0]:
            raise ValueError("Sorry, the input data for 'first_piece' and 'tiling' do not match.")

        if isinstance(node, np.ndarray):
            if not node.shape[0] == node.shape[1]:
                raise ValueError("Sorry, the input data for 'node' has not the correct shape. It needs to be square.")

    @property
    def root(self) -> LowerTriangularFactorisedBlockMatrixNode:

        """Property method for the root.

        This is the first node of a chain which we call a tree even if it's structure is straightforward."""

        return self._root

    @property
    def tiling(self) -> IntegerList:

        """Property method for the tiling."""

        return self._tiling

    # Computed properties

    @property
    def positions(self):

        """Property method for the positions.

        These are essentially given by the cumulative sum of the tiling."""

        return self._tiling.cumsum()

    @property
    def shape(self) -> Tuple[INT_DTYPE, INT_DTYPE]:

        """Property method the shape.

        The shape is given by the sum of the tiling."""

        N = np.sum(self._tiling.entries)

        return (N, N)

    @property
    def max_depth(self) -> INT_DTYPE:

        """Property method for the maximal possible factorisation depth.

        It equals the depth of the underlying tiling."""

        return self._tiling.depth()

    @property
    def depth(self) -> INT_DTYPE:

        """Computes the current depth of the factorisation chain.

        It is given by counting the amount of nodes including the root and not the leaf.
        This can be also understood as the amount of edges in the corresponding graph."""

        depth = 1
        node = self._root

        while (not node.is_leaf):
            node = node.node
            depth += 1

        return depth

    @property
    def tree(self) -> list:

        """Computes the L-Factorisation Chain w.r.t to the current depth.

        It is given by traversing through all nodes from the root to the leaf.
        THIS METHOD USES APPEND AND MIGHT BE SLOWER THAN EXPECTED.

        Notes:
        ------
        1. TODO: Change naming of ltfbm.tree to ltfbm.chain"""

        tree = []
        knot = self._root

        while (not knot.is_leaf):
            tree.append(knot.first_piece)
            knot = knot.node

        tree.append(knot.first_piece)
        tree.append(knot.leaf_factors)

        return tree

    @property
    def bytes(self) -> INT_DTYPE:

        """Property method for storage size in bytes.

        It is given by the storage size of the tiling plus the storage size of all nodes including the root and the leaf."""

        tiling_size = sys.getsizeof(self.tiling)
        root_size = sys.getsizeof(self._root)

        first_pieces_size = 0
        leaf_factors_size = 0

        node = self._root

        while not node.is_leaf:
            first_pieces_size += node.first_piece.bytes
            node = node.node

        leaf_factors_size = node.leaf_factors.bytes

        return tiling_size + root_size + first_pieces_size + leaf_factors_size

    # COMPLEX CLASS METHODS
    
    def item(self, row: INT_DTYPE, col: INT_DTYPE) -> FLOAT_DTYPE:

        """Gives back the item w.r.t. row, col.

        This method is using a JIT compiled method: jit_ltfbm_item.

        Args:
        -----
            row: INT_DTYPE
                The row.

            col: INT_DTYPE
                The column.
                
        Returns:
        --------
            FLOAT_DTYPE:
                Entry at ``row``, ``col`` of the original matrix."""

        warnings.filterwarnings("ignore", category=numba.errors.NumbaPendingDeprecationWarning)

        tiling = self._tiling

        chain = [item.entries for item in self.tree]

        return jit_ltfbm_item(
            row = row,
            col = col,
            tiling = tiling,
            chain = chain)

    def factorise(self, max_depth:INT_DTYPE = np.inf, protocol:bool = False):

        """Factorises the present chain.

        This method does not check if the matrix is reconstructable from its L-Factorisation chain.

        Args:
        -----
            max_depth: INT_DTYPE
                The maximal depth is the depth where the factorisation should terminate.

            protocol: bool
                Should the protocol be printed during the factorisation? Y/N

            
        Raises:
        -------
            ValueError:
                If ``max_depth`` has not the specified type.

            ValueError:
                If ``protocol`` has not the specified type.

        Returns:
        --------
            bool:
                If the L-factorisation was successful. 
            
            INT_DTYPE:
                If the L-factorisation was extended, how deep it extended it (total number of iterations)."""

        if (not (isinstance(max_depth, int) | isinstance(max_depth, float))):
            raise ValueError("Sorry, your input data for 'max_depth' has not the correct type.")

        elif not isinstance(protocol, bool):
            raise ValueError("Sorry, your input data for 'protocol' has not the correct type.")

        return self._root.factorise(
            positions = self.positions.entries,
            max_depth = max_depth,
            protocol = protocol
        )

    def dot(self, vec: ARRAY, deep_compatible: bool=False)->ARRAY:

        """Compute the matrix-vector product by utilising (deep) compatibility of the tiling with the ltfbm format.

        Args:
        -----
            vec: ARRAY
                The vector to be multiplied with the matrix.

            deep_compatible: bool
                Should the deep compatibility be used? Y/N

        Returns:
        --------
            ARRAY:
                The result of the matrix-vector product."""

        warnings.filterwarnings("ignore", category=numba.errors.NumbaPendingDeprecationWarning)

        tiling = self._tiling
        entropy = tiling.entropy()
        tree = [item.entries for item in self.tree]

        if not deep_compatible:

            return jit_ltfbm_dot_compatible(
                tiling = tiling,
                tree = tree,
                entropy = entropy.entries.tolist(),
                vec = vec
            )

        else:

            return jit_ltfbm_dot_deep_compatible(
                tree = tree,
                til = tiling.entries,
                vec = vec,
                e_scheme = EntropyScheme(),
                dcf = DeepCompatibleIntegerList(),
                dcl = DeepCompatibleIntegerList()
            )

    def reconstruct(self, max_depth: INT_DTYPE=np.infty)->RowMajorOrdering:

        """Reconstructs up to a pre-specified max_depth.

        THIS METHOD CALLS DEPTH TIMES A JIT COMPILED FUNCTION AND THUS MIGHT BE SLOWER.

        Args:
        -----
            max_depth: INT_DTYPE
                The maximal depth where the reconstruction should terminate.

        Raises:
        -------
            ValueError:
                If ``max_depth`` has not the specified type.

        Returns:
        --------
            RowMajorOrdering:
                The reconstructed matrix up to the specified max_depth (down to top / leaf to root - wise)."""

        if (not (isinstance(max_depth, int) | isinstance(max_depth, float))):
            raise ValueError("Sorry, your input data for 'max_depth' has not the correct type.")

        warnings.filterwarnings("ignore", category=numba.errors.NumbaPendingDeprecationWarning)

        res = None

        for i, rmo in enumerate(reversed(self.tree)):
            if i > max_depth:
                break

            if res is None:
                res = rmo
            else:
                res = RowMajorOrdering(
                        np.block(jit_ltfbm_reconstruct(
                            first_piece_entries = rmo.entries,
                            leaf_factors_entries = res.entries,
                            tiling = self._tiling.entries[0:res.size]
                    )))

        return res
    
if __name__ == "__main__":

    pass
