""" This module contains the Chain class. """

class Chain:

    def __init__(self, element, lambda_expr):

        """Initialises the chain object.

        :param element: Element of the chain.
        :param lambda_expr: Lambda expression defining the chain.

        Notes:
        ------
        1. The chain object is a wrapper for a lambda expression which can be applied to an element of the chain.
        2. The lambda expression should return the next element of the chain or None if the chain is finished.
        3. If the lambda expression induces an infinite loop, the react method will not terminate."""


        self._element = element
        self._lambda_expr = lambda_expr


    @property
    def element(self):

        """Getter for the element of the chain."""

        return self._element

    def next(self):

        """ Computes the next element of the chain.

        :return: True if the next element is not None, False otherwise."""


        next_element = self._lambda_expr(self._element)

        if next_element is None:

            return False

        else:

            self._element = next_element

            return True

    def react(self):

        """ Computes the reaction of the chain from the current element on.

        :return: List of elements of the chain from the current element on."""

        # Initiliase tree

        res = [self._element]

        # Use next method

        while self.next():

            res.append(self._element)

        # Return result

        return res

if __name__ == "__main__":

    pass