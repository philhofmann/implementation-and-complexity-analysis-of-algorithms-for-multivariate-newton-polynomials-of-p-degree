import numpy as np
import minterpy as mp
from nexpy.core.triangular import IntegerList, Index
from nexpy.global_settings import ARRAY, INT_DTYPE, FLOAT_DTYPE
from nexpy.jit_compiled_utils import get_exponent_matrix_iterative_subroutine, get_exponent_matrix_iterative_tilings_only_subroutine, extract_tiling_from_exponent_matrix_subroutine

""" This module contains the utility functions of the NexPy package.

Notes
-----
- The following functions are not (yet) included in the tests."""

def _get_exponent_matrix_catch_edge_case(
    method: callable, spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE, tilings_only: bool) -> ARRAY:

    """Helper function to catch edge cases for the get_exponent_matrix_iterative function. 

    Args:
    -----
        method: callable
            The method to be used.

        spatial_dimension: INT_DTYPE
            The spatial dimension.

        poly_degree: INT_DTYPE
            The polynomial degree.

        p_degree: FLOAT_DTYPE
            The p degree.

        tilings_only: bool
            Whether to return the tiling only.

    Raises:
    -------
        ValueError: 
            If the poly_degree parameter is negative.

        ValueError:
            If the p_degree parameter is negative.

    Returns:
    --------
        ARRAY:
            The exponent matrix."""

    if poly_degree < 0:
            raise ValueError("The poly_degree parameter should be non-negative.")

    if p_degree < 0.0:
            raise ValueError("The p_degree parameter should be non-negative.")
    
    exponents = None

    if 0.0 < p_degree and p_degree <= 2.0:

        return method(
            spatial_dimension = spatial_dimension,
            poly_degree = poly_degree,
            p_degree = p_degree)

    elif p_degree == 0.0:

        exponents = np.zeros((spatial_dimension*poly_degree + 1, spatial_dimension))
        k = 1

        for m in range(0, spatial_dimension):

            for n in range(1, poly_degree + 1):

                exponents[k][m] = n

                k += 1

    elif p_degree > 2.0:

        exponents = get_exponent_matrix_conditional(
            spatial_dimension = spatial_dimension,
            poly_degree = poly_degree,
            p_degree = p_degree)

    if exponents is None:
        raise Exception("Sorry, but the exponent matrix could not be computed.")
    
    if not tilings_only:
        return exponents
    
    else: 
        return extract_tiling_from_exponent_matrix(exponents)
    
def get_exponent_matrix_iterative(
    spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE, tilings_only = False) -> ARRAY:

    """Computes the exponent matrix iteratively.

    This method increments carry-wise and checks the condition for the p-degree while iterating.
    
    Args:
    ----
        spatial_dimension: INT_DTYPE
            The spatial dimension.

        poly_degree: INT_DTYPE
            The polynomial degree.

        p_degree: FLOAT_DTYPE
            The p degree.

        tilings_only: bool
            Whether to return the tiling only.
        
    Raises:
    ------
        ValueError: 
            If the spatial_dimension parameter is less than 1.

        ValueError:
            If the poly_degree parameter is negative.

        ValueError:
            If the p_degree parameter is negative.

        ValueError:
            If the tilings_only parameter is True and the spatial_dimension parameter is 1.
            
    Returns:
    -------
        ARRAY:
            The exponent matrix for the given parameters."""

    if spatial_dimension < 1:

        raise ValueError("The spatial_dimension parameter should be at least 1.")
    
    if p_degree < 0.0:

        raise ValueError("The p_degree parameter should be non-negative.")
    
    if poly_degree < 0:

        raise ValueError("The poly_degree parameter should be non-negative.")

    method = get_exponent_matrix_iterative_subroutine

    if tilings_only:

        if spatial_dimension == 1:

            raise ValueError("The tilings_only parameter is not supported for spatial_dimension = 1.")

        method = get_exponent_matrix_iterative_tilings_only_subroutine
    

    return _get_exponent_matrix_catch_edge_case(
        method = method,
        spatial_dimension = spatial_dimension,
        poly_degree = poly_degree,
        p_degree = p_degree,
        tilings_only=tilings_only)

def get_exponent_matrix_conditional(spatial_dimension: INT_DTYPE, poly_degree: INT_DTYPE, p_degree: FLOAT_DTYPE) -> ARRAY:

    """Computes the exponent matrix conditionally.

    This method computes the flipped cartesian product of {0, 1, ..., poly_degree}^spatial_dimension first and after that applies the condition for the p-degree.
    
    Args:
    -----
        spatial_dimension: INT_DTYPE
            The spatial dimension.

        poly_degree: INT_DTYPE
            The polynomial degree.
            
        p_degree: FLOAT_DTYPE
            The p degree.
        
    Raises:
    -------
        ValueError: 
            If the spatial_dimension parameter is less than 1.

        ValueError:
            If the poly_degree parameter is negative.

        ValueError:
            If the p_degree parameter is negative.
    
    Returns:
    --------
        ARRAY:
            The exponent matrix for the given parameters."""

    if spatial_dimension < 1:

        raise ValueError("The spatial_dimension parameter should be at least 1.")
    
    if p_degree < 0.0:

        raise ValueError("The p_degree parameter should be non-negative.")
    
    if poly_degree < 0:

        raise ValueError("The poly_degree parameter should be non-negative.")
    
    cart_rev = np.flip(mp.utils.cartesian_product(
            *[np.arange(poly_degree + 1, dtype=INT_DTYPE)] * spatial_dimension), axis=1)
    
    if p_degree == np.infty:

        return cart_rev
    
    else:

        cond = np.sum(cart_rev**p_degree, axis=1) <= poly_degree**p_degree

        return cart_rev[cond]

def extract_tiling_from_exponent_matrix(exp_mat: ARRAY) -> ARRAY:

    """ Extracts the tiling from the exponent matrix.
    
    Args:
    ----
        exp_mat: ARRAY
            The exponent matrix.
        
    Raises:
    -------
        ValueError: 
            If the exp_mat parameter is None.

        ValueError:
            If the exp_mat parameter has not the right type.

        ValueError:
            If the exp_mat parameter is not a 2D array.

        ValueError:
            If the exp_mat parameter has no rows.

        ValueError:
            If the exp_mat parameter has no columns.
    
    Returns:
    --------
        ARRAY:
            The tiling of the exponent matrix."""
    
    if exp_mat is None:
            
        raise Exception("The exponent matrix (exp_mat) is None.")

    if not isinstance(exp_mat, ARRAY):

        raise Exception("The exponent matrix (exp_mat) has not the right type.")

    if not len(exp_mat.shape) == 2:

        raise Exception("The exponent matrix (exp_mat) is not a 2D array.")
    
    if exp_mat.shape[0] == 0:

        raise Exception("The exponent matrix (exp_mat) has no rows.")
    
    if exp_mat.shape[1] == 0:

        raise Exception("The exponent matrix (exp_mat) has no columns.")

    m = exp_mat.shape[1]

    exp_mat_mod = np.concatenate([np.zeros((1, m), dtype=INT_DTYPE), np.diff(exp_mat, axis=0)])

    prototype = np.append(np.array([1]), np.zeros(m-1))

    lst = np.all(exp_mat_mod == prototype, axis=1).astype(INT_DTYPE)

    return extract_tiling_from_exponent_matrix_subroutine(lst=lst)

def chebychev_2nd_order(n: INT_DTYPE) -> ARRAY:

    """ Chebyshev 2nd order points
    
    Args:
    -----
        n: INT_DTYPE
            Number of points.
    
    Returns:
    --------
        ARRAY: 
            Chebyshev 2nd order points.
        
    Raises:
    -------
        ValueError: 
            If the parameter ``n`` is negative.
            
    Notes:
    ------
    - NOTE: This method is currently only used for the fast Newton polynomial."""
    
    if n < 0:
        raise ValueError("The parameter ``n`` should be non-negative.")

    if n == 0:
        return np.zeros(1, dtype=FLOAT_DTYPE)
    
    if n == 1:
        return np.array([-1.0, 1.0], dtype=FLOAT_DTYPE)
    
    return np.cos(np.arange(n, dtype=FLOAT_DTYPE) * np.pi / (n - 1))

def compute_restoration_matrix(L: IntegerList) -> ARRAY:

    """ Computes the restoration matrix for a given tiling.

    This is the deep single value restoration algorithm.
    
    Args:
    -----
        L: IntegerList
            The tiling.
        
    Returns:
    --------
        ARRAY:
            The restoration matrix."""

    matrix = []
    e = L.entropy().entries
    e_redu = e[:-1]
    size = L.size

    for i in range(size):
        row = []
        for j in range(size):
            k = np.max([i, j])
            size = np.min(e_redu[e_redu >= k+1])
            depth = len(e <= size) - 1
            
            row_index, col_index = Index(i), Index(j)
            restoration = np.array([row_index.restoration(L, depth),
                                    col_index.restoration(L, depth)]).T
            row.append(restoration)
            
        matrix.append(row)

    return np.array(matrix)

def moving_average(arr: ARRAY, k : INT_DTYPE):

    """Compute the moving average of an array. 

    Args
    ----
        arr: ARRAY
            The input array.
        k: INT_DTYPE
            The window size.

    Returns
    -------
        ARRAY:
            The moving average of the input array with window size k.
    
    Notes
    -----
    - NOTE: This is currently only used for smoothing experimental results."""

    if k == 1:

        return arr

    # Define a kernel for the convolution operation
    kernel = np.ones(k) / k

    # Use the convolution operation to compute the moving average
    moving_avg = np.convolve(arr, kernel, mode='valid')

    # Return moving average
    return moving_avg

def array_ratio(arr1, arr2) -> ARRAY:

    """ Compute the ratio of two arrays.

    Args:
    -----
        arr1: ARRAY
            The first array which is the numerator.

        arr2: ARRAY
            The second array which is the denominator.

    Raises:
    -------
        ValueError: 
            If arr1 or arr2 has not the right type.
        
        ValueError:
            If arr1 and arr2 have not the same shape.
    
    Returns:
    --------
        ARRAY:
            The ratio of the two arrays which means arr1[i]/arr2[i] for all i in [0, n-1] where n is the length of the arrays.

    Notes:
    ------
    - NOTE: This is currently only used for computing the ratio in experiments.py"""

    # Type checks
    type_check_1 = isinstance(arr1, list) | isinstance(arr1, ARRAY)
    type_check_2 = isinstance(arr2, list) | isinstance(arr2, ARRAY)
    type_check = type_check_1 & type_check_2

    # Raise type check exception

    if not type_check:
        raise ValueError("Sorry, your input data has not the right type.")

    # Format check
    n = len(arr1)
    format_check = n == len(arr2)

    if not format_check:
        raise ValueError("Sorry, but make sure that arr1 and arr2 have the same shape.")

    # Data type conversion
    if not isinstance(arr1, ARRAY):
        arr1 = np.array(arr1)

    if not isinstance(arr2, ARRAY):
        arr2 = np.array(arr2)

    # Compute arrays ratio
    ratios = np.zeros(n)

    for i in range(n):
        if not arr2[i] == 0.0:
            ratios[i] = arr1[i]/arr2[i]

        else:
            ratios[i] = np.infty

    # Return ratios
    return ratios

if __name__ == "__main__":
    
    pass

