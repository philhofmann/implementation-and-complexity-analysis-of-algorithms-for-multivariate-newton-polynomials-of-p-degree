import time
import numpy as np

from typing import List
from nexpy.global_settings import INT_DTYPE, ARRAY
from nexpy.utils import get_exponent_matrix_conditional, extract_tiling_from_exponent_matrix
from nexpy.core.triangular.ltfbm import LowerTriangularFactorisedBlockMatrix
from nexpy.core.triangular.integer_list import IntegerList, integer_list_reduction_chain

""" This module contains the random utility functions of the NexPy package. 

Notes:
------
- NOTE: The following functions are not (yet) included in the tests."""

def integer_list(size: INT_DTYPE):

    """ Generates a pseudo random (dominated non-trivial) integer list with a given size. 

    A dominated non-trivial integer list is a list of integers with at all entries greater zero and dominated by the first entry.
    
    Args:
    -----
        size: INT_DTYPE
            The size of the integer list which is equal to the sum of the entries of the integer list.

    Returns:
    --------
        IntegerList
            A pseudo random dominated non-trivial integer list."""

    n = int(np.random.rand()*(size-2)+2)
    L = [n]
    L_sum = n

    while L_sum < size:
        min_nxt = min([size-L_sum, n])
        nxt = int(np.random.rand()*min_nxt+1)
        L.append(nxt)
        L_sum = sum(L)

    return IntegerList(np.array(L))

def deep_compatible_integer_list(depth, n=None, max_n=5) -> ARRAY:

    """ Generates a pseudo random deeply compatible integer list with a given depth and a given parameter ``n``.
    
    Args:
    -----
        depth: INT_DTYPE
            The depth of the integer list.

        n: INT_DTYPE
            The polynomial degree in order to generate the integer list by the tilings of the exponent matrix.

        max_n: INT_DTYPE
            The maximum polynomial degree to generate the integer list.

    Returns:
    --------
        IntegerList
            A pseudo random deeply compatible integer list."""
    
    if n is None:

        n = int(np.random.rand()*(max_n-2)+2)

    p = [0.0625, 0.125, 0.25, 0.5, 1.0, 2.0, 4.0, np.infty][np.random.randint(8)]

    return IntegerList(
        extract_tiling_from_exponent_matrix(get_exponent_matrix_conditional(spatial_dimension=depth, poly_degree=n, p_degree=p)))

def uncertain_integer_lists(n, max_give_back=10, total_amount=5000, min_depth=3, max_depth=10, compatible=False) -> List[IntegerList]:

    """ Generates uncertain amount of random integer lists with uncertain depth between min_depth and max_depth.

    It gives back a maximum of max_give_back lists and stops after total_amount lists have been tested.
    All lists have the same size equal to size.

    Args:
    -----
        n: INT_DTYPE
            The size of the integer lists.

        max_give_back: INT_DTYPE
            The maximum amount of integer lists to give back.

        total_amount: INT_DTYPE
            The total amount of integer lists to test.
        
        min_depth: INT_DTYPE
            The minimum depth of the integer lists.
        
        max_depth: INT_DTYPE
            The maximum depth of the integer lists.

        compatible: bool
            If True, the integer lists given back will be compatible.

    Returns:
    --------
        List[IntegerList]:
            A list of pseudo randomly generated integer lists.

    Notes:
    -------
    - This method is deprecated."""
    
    ils = []

    for _ in range(total_amount):

        il = integer_list(n)

        il_depth = il.depth()

        if max_depth >= il_depth and il_depth >= min_depth:

            if not compatible:

                ils.append(il)

            else:

                if il.is_compatible():

                    ils.append(il)

        if len(ils) == max_give_back:

            return ils

    return ils

def ltfbm(tiling: IntegerList, max_depth = None, monitoring=False):

    """ Generates a random lower triangular factorised block matrix from a given integer list.

    Args:
    -----
        tiling: IntegerList
            The integer list to generate the lower triangular factorised block matrix from.
        
        max_depth: INT_DTYPE
            The maximum depth of the integer list to consider.

        monitoring: bool
            If True, the time in milliseconds is given back.

    Raises:
    -------
        ValueError
            If the depth of the integer list is 1.
            
    Returns:
    --------
        LowerTriangularFactorisedBlockMatrix
            A lower triangular factorised block matrix and the time in milliseconds if monitoring is True."""

    # Compute depth
    depth = tiling.depth()

    if max_depth is not None:
        depth = min(depth, max_depth)

    # Compute first piece size
    n = tiling.entries[0]

    if depth == 1:
        raise ValueError("The depth of the integer list is 1, but it should be at least 2.")

    leaf_factors_size = tiling.entropy().entries[depth - 1]

    # Generate random first_pieces and leaf_factors

    rand_first_pieces = np.random.rand(depth - 1, n, n)*10
    rand_leaf_factors = np.random.rand(leaf_factors_size, leaf_factors_size)*10
    ltfbm = None

    # Construct from Factorised RMO
    start = time.time()

    # Reaction of reduction chain
    reduchain = integer_list_reduction_chain(tiling).react()

    for idx_d in range(1, depth):
        fp = rand_first_pieces[-idx_d]

        if idx_d == 1:
            node = rand_leaf_factors

        else:
            node = ltfbm.root

        ltfbm = LowerTriangularFactorisedBlockMatrix(
            first_piece = fp,
            node = node,
            tiling = reduchain[depth - idx_d - 1])
    
    end = (time.time() - start) * 1e3

    if monitoring:
        return ltfbm, end

    else:
        return ltfbm

if __name__ == "__main__":

    pass

   