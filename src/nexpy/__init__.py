__version__ = '0.1'

# Folders

from . import core
from . import visual

# Files

from . import global_settings
from . import jit_compiled_utils
from . import random_utils
from . import utils

# Classes

from .fast_newton_poly import FastNewtonPolynomial

# Callables 

from .fast_newton_transformations import fnt, ifnt, fntn, ifntn