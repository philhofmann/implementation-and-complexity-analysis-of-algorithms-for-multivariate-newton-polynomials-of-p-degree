from nexpy.global_settings import ARRAY


""" This module contains the fast Newton transformations of the NexPy package. 

Note:
-----
1. The following functions are not included yet but planned to be included in the future."""


def fnt(a: ARRAY) -> ARRAY:

    """ Computes the fast Newton transformation of a given input array.
    
    :param a: The input array."""

    raise NotImplementedError("This function is not yet implemented.")

def ifnt(a: ARRAY) -> ARRAY:

    """ Computes the inverse fast Newton transformation of a given input array.
    
    :param a: The input array."""

    raise NotImplementedError("This function is not yet implemented.")

def fntn(a: ARRAY) -> ARRAY:

    """ Computes the n-dimensional fast Newton transformation of a given input array.
    
    :param a: The input array."""

    raise NotImplementedError("This function is not yet implemented.")

def ifntn(a: ARRAY) -> ARRAY:

    """ Computes the inverse n-dimensional fast Newton transformation of a given input array.
    
    :param a: The input array."""

    raise NotImplementedError("This function is not yet implemented.")


if __name__ == "__main__":
    
    pass