from setuptools import setup, find_packages

if __name__ == "__main__":
    setup(
        name='nexpy',
        author='Phil Hofmann',
        author_email='philhofmann@outlook.com',
        description='A package for fast multivariate Newton interpolation.', 
        python_requires='>=3.8',
        version='0.1',
        packages=find_packages(where='src'),
        package_dir={'': 'src'},
        install_requires=[
            'numpy',
            'matplotlib',
            'numba',
            'IPython',
            'sphinx',
            'nbsphinx',
            'scipy',
            'notebook',
            'minterpy @ git+https://github.com/casus/minterpy@main', # do not add in environment.yml !
        ],
    )
