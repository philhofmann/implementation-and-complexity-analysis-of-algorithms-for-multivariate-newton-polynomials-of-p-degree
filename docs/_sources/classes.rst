Classes
=======

.. toctree::
   :maxdepth: 1

   classes/FastNewtonPolynomial
   classes/Chain
   classes/RowMajorOrdering
   classes/IntegerList
   classes/Index
   classes/DeepCompatibleIntegerList
   classes/EntropyScheme
   classes/LowerTriangularFactorisedBlockMatrix
   classes/LowerTriangularFactorisedBlockMatrixNode
