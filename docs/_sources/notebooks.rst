Notebooks
=========

.. toctree::
   :maxdepth: 1

   notebooks/experiments
   notebooks/runge function 2D
   notebooks/runge function 3D
   notebooks/time complexities
   notebooks/triangular