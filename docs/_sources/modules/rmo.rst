rmo.py
======

.. automodule:: nexpy.core.triangular.rmo
   :members: jit_rmo_dot, jit_rmo_reconstruct
