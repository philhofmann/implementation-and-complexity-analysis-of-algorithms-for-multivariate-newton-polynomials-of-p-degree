ltfbm.py
========

.. automodule:: nexpy.core.triangular.ltfbm
   :members: jit_ltfbm_item, jit_ltfbm_dot_deep_compatible, jit_ltfbm_dot_compatible, jit_ltfbm_reconstruct
