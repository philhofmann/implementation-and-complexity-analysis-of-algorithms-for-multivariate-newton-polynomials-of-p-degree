RowMajorOrdering
================

.. autoclass:: nexpy.core.triangular.RowMajorOrdering
   :members:
   :private-members:
   :show-inheritance: