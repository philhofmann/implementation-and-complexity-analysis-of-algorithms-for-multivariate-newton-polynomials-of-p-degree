DeepCompatibleIntegerList
=========================

.. autoclass:: nexpy.core.triangular.DeepCompatibleIntegerList
   :members:
   :private-members:
   :show-inheritance: