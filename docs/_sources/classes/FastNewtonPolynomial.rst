FastNewtonPolynomial
======================

.. autoclass:: fast_newton_poly.FastNewtonPolynomial
   :members:
   :private-members:
   :show-inheritance: