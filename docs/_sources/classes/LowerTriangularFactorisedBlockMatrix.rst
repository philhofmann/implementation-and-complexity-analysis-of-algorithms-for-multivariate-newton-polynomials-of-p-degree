LowerTriangularFactorisedBlockMatrix
====================================

.. autoclass:: nexpy.core.triangular.LowerTriangularFactorisedBlockMatrix
   :members:
   :private-members:
   :show-inheritance: