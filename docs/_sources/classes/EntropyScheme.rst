EntropyScheme
=============

.. autoclass:: nexpy.core.triangular.EntropyScheme
   :members:
   :private-members:
   :show-inheritance: