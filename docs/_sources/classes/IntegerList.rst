IntegerList
===========

.. autoclass:: nexpy.core.triangular.IntegerList
   :members:
   :private-members:
   :show-inheritance: