LowerTriangularFactorisedBlockMatrixNode
========================================

.. autoclass:: nexpy.core.triangular.LowerTriangularFactorisedBlockMatrixNode
   :members:
   :private-members:
   :show-inheritance: