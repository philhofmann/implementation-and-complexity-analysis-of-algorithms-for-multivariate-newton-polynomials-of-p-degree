Fast Newton Polynomial
======================

.. autoclass:: fast_newton_poly.FastNewtonPolynomial
   :members:
   :private-members:
   :show-inheritance:
