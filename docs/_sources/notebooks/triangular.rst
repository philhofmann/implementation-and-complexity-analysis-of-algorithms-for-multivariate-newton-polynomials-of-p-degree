Triangular
==========

.. toctree::
   :maxdepth: 1

   src/triangular/Motivation Multi-Index Set
   src/triangular/Multi-Index Sets
   src/triangular/Integer List
   src/triangular/Lower Triangular (Fully) Factorised Matrix
   src/triangular/(Deep) Reconstruction
   src/triangular/Index
   src/triangular/(Deep) Compatible Matrix-Vector Product
   
   