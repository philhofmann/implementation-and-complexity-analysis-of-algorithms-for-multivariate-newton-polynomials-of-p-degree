Compatible Matrix-Vector Product
================================

.. toctree::
   :maxdepth: 1

   ../notebooks/time complexities/Compatible MVP
