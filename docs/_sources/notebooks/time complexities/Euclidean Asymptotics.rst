Euclidean Asymptotics
=====================

.. toctree::
   :maxdepth: 1

   ../notebooks/time complexities/Euclidean Asymptotics
