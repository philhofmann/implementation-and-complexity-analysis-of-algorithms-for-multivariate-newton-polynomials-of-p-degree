Runge Function 2D
=================

.. toctree::
   :maxdepth: 1

   src/runge function 2D/Fast Newton Polynomial
   src/runge function 2D/Newton Polynomial
   src/runge function 2D/Fast Fourier Transformation