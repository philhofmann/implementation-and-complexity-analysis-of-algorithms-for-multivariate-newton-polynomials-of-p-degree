Runge Function 3D
=================

.. toctree::
   :maxdepth: 1

   src/runge function 3D/Fast Newton Polynomial
   src/runge function 3D/Newton Polynomial
   src/runge function 3D/Fast Fourier Transformation