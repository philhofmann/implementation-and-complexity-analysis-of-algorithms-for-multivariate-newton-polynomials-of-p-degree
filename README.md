# Implementation and Complexity Analysis of Algorithms for Multivariate Newton Polynomials of p-Degree

(Newton Express Interpolation in Python)

A package for fast multivariate Newton interpolation.

## Authors

- [Phil-Alexander Hofmann](https://gitlab.com/philippo_calippo) - [CASUS](https://www.casus.science/) ([HZDR](https://www.hzdr.de/))

## Acknowledgments

I  would like to acknowledge:
- [Prof. Dr. Peter Stadler](https://www.uni-leipzig.de/personenprofil/mitarbeiter/prof-dr-peter-florian-stadler) - University of Leipzig,
- [Prof. Dr. Michael Hecht](https://www.casus.science/de-de/team-members/michael-hecht/) - [CASUS](https://www.casus.science/) ([HZDR](https://www.hzdr.de/)),
- [Dr. Damar Wicakson](https://www.casus.science/de-de/team-members/dr-damar-wicaksono/) - [CASUS](https://www.casus.science/) ([HZDR](https://www.hzdr.de/)),

and the support and resources provided by the [Center for Advanced Systems Understanding](https://www.casus.science/) ([Helmholtz-Zentrum Dreden-Rossendorf](https://www.hzdr.de/))  where the development of this project took place.

## 📜 License

The project is licensed under the [MIT License](LICENSE.txt). 

## 📖 Citations

* CASUS. [Minterpy](https://github.com/casus/minterpy). 2024. Licensed under the [MIT](https://github.com/casus/minterpy/blob/main/LICENSE) License.
* <a target="_blank" href="https://icons8.de/icon/H6KtH9DAxZGm/science">Science</a> Icon von <a target="_blank" href="https://icons8.com">Icons8</a>

## 🚧 Project status 🚧

The original project repository has been deprecated/archived as development has shifted to a new repository located at [NexPy](https://gitlab.com/philhofmann/nexpy). Please refer to the new repository for the latest updates and contributions.

## 💻 Installation

Please follow the steps below:

1. Clone the project:
```bash
git clone https://gitlab.com/philhofmann/implementation-and-complexity-analysis-of-algorithms-for-multivariate-newton-polynomials-of-p-degree.git
```

2. Create a virtual environment:
```bash
conda env create -f environment.yml
```

3. Activate environment:
```bash
conda activate nexpy
```

4. Install using pip:
```bash
pip install -e .
```

5. If you want to deactivate the environment:
```bash
conda deactivate
```
