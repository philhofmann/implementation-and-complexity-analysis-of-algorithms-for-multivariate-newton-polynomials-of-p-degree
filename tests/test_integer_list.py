import unittest
import numpy as np

from nexpy.random_utils import integer_list, deep_compatible_integer_list
from nexpy.core.triangular.integer_list import IntegerList, integer_list_reduction_chain

"""Test the IntegerList class.

Notes:
-----
1. The tests are not exhaustive.
2. We do not test to string methods.
3. We do not test whether the operations are all correctly fed into the history."""

class TestIntegerList(unittest.TestCase):

    def test_entries_getter(self):

        """Test the entries getter of the integer_list_class."""

        entries = np.random.randint(1, 100, size = np.random.randint(10, 100))

        il = IntegerList(entries)

        self.assertTrue(np.array_equal(il.entries, entries))

    def test_history_empty_getter(self):

        """Test the history getter of the integer_list_class when the history is empty."""

        il = integer_list(size=np.random.randint(10, 100))

        self.assertTrue(il.history.size == 0)

    def test_length_getter(self):

        """Test the length getter of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        self.assertEqual(il.length, il.entries.size)

    def test_size_getter(self):

        """Test the size getter of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        self.assertEqual(il.size, np.sum(il.entries))

    def test_is_non_trivial(self):
            
        """ Test the is non trivial method of the integer_list_class. """

        il = integer_list(size=np.random.randint(10, 100))

        self.assertTrue(il.is_non_trivial)

        il = IntegerList(np.array([3,2,1,2,1,1,2,1,1,0]))

        self.assertFalse(il.is_non_trivial)

    def test_is_dominated(self):

        """ Test the is dominated method of the integer_list_class. """

        il = integer_list(size=np.random.randint(10, 100))

        self.assertTrue(il.is_dominated)

        il = IntegerList(np.array([3,2,1,2,1,1,2,1,1,4]))

        self.assertFalse(il.is_dominated)

    def test_is_monotonous(self):

        """ Test the is monotonous method of the integer_list_class. """

        il = IntegerList(np.array([3,2,1,2,1,1,2,1,1,1]))

        self.assertFalse(il.is_monotonous)

        il = IntegerList(np.array([9,8,7,6,5,2,1]))

        self.assertTrue(il.is_monotonous)
    
    def test_append_history(self):

        """Test the append history method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        new_value = np.random.randint(1, 100)

        il._append_history(new_value)

        self.assertEqual(il.history, np.array([new_value], dtype = np.int64))

    def test_copy_history(self):

        """Test the copy history method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        new_value = np.random.randint(1, 100)

        il._append_history(new_value)

        il_copy = IntegerList(np.array([0]))

        il_copy._copy_history(il.history)

        self.assertEqual(il.history, il_copy.history)

    def test_item(self):

        """Test the item method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        index = np.random.randint(0, il.entries.size)

        self.assertEqual(il.item(index), il.entries[index])

    def test_where(self):

        """Test the where method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        value = il.entries[np.random.randint(1, il.entries.size)]

        self.assertEqual(il.where(value), np.where(il.entries == value)[0][0])

    def test_flip_order(self):

        """Test the flip order method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        self.assertTrue(np.array_equal(il.flip_order().entries, il.entries[::-1]))

    def test_flip_sign(self):

        """Test the flip sign method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        self.assertTrue(np.array_equal(il.flip_sign().entries, -il.entries))

    def test_differences(self):

        """Test the differences method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        differences = np.diff(np.append(il.entries, 0))

        self.assertTrue(np.array_equal(il.differences().entries, differences))

    def test_cumsum(self):

        """Test the cumulative sum method of the integer_list_class."""

        il = integer_list(size=np.random.randint(10, 100))

        cumsum = np.cumsum(il.entries)

        self.assertTrue(np.array_equal(il.cumsum().entries, cumsum))

    def test_sub(self):
            
        """Test the sub method of the integer list class. """

        il = integer_list(size=np.random.randint(10, 100))

        length = il.length

        start = np.random.randint(1, length)

        end = np.random.randint(start, length)

        self.assertTrue(np.array_equal(il.sub(start, end).entries, il.entries[start:end]))

    def test_apply(self):
            
        """ Test the apply method of the integer list class. """

        il1 = IntegerList(np.array([3,2,1,2,1,1,2,1,1,1]))

        il2 = IntegerList(np.array([3,2,1,2,1,1]))

        self.assertTrue(np.array_equal(il1.apply(il2).entries, np.array([6,3,1,3,1,1])))

        il1 = IntegerList(np.array([6,3,1,3,1,1]))

        il2 = IntegerList(np.array([3, 2, 1]))

        self.assertTrue(np.array_equal(il1.apply(il2).entries, np.array([10,4,1])))

        il1 = IntegerList(np.array([10,4,1]))

        il2 = IntegerList(np.array([3]))

        self.assertTrue(np.array_equal(il1.apply(il2).entries, np.array([15])))

    def test_entropy(self):

        """Test the entropy method of the integer_list_class."""

        entries = np.array([3, 2, 1, 2, 1, 1, 2, 1, 1, 1])

        il = IntegerList(entries)

        entropy = np.array([15, 10, 6, 3, 1])

        self.assertTrue(np.array_equal(il.entropy().entries, entropy))

    def test_depth(self):

        """Test the depth method of the integer_list_class."""

        depth = np.random.randint(2, 5)

        il = deep_compatible_integer_list(depth = depth)

        self.assertEqual(il.depth(), depth)

    def test_reduction(self):
            
        """Test the reduction method of the integer_list_class."""

        entries = np.array([3, 2, 1, 2, 1, 1, 2, 1, 1, 1])

        il = IntegerList(entries)

        reduction = np.array([3, 2, 1, 2, 1, 1])

        self.assertTrue(np.array_equal(il.reduction().entries, reduction))

    def test_is_compatible(self):

        """Test the is compatible method of the integer_list_class."""

        for depth in range(2, 6):

            il = deep_compatible_integer_list(depth = depth)

            self.assertTrue(il.is_compatible())

        il = IntegerList(np.array([3, 2, 1, 2, 1, 2]))

        self.assertTrue(il.is_compatible())

        il = IntegerList(np.array([3, 2, 1, 4, 1, 1]))

        self.assertFalse(il.is_compatible())

        il = IntegerList(np.array([3, 2, 1, 2, 1, 0]))

        self.assertFalse(il.is_compatible())

    def test_is_deep_compatible(self):

        """Test the is deep compatible method of the integer_list_class."""

        for depth in range(2, 6):

            il = deep_compatible_integer_list(depth = depth)

            self.assertTrue(il.is_deep_compatible())

        il = IntegerList(np.array([5, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1]))

        self.assertFalse(il.is_deep_compatible())

if __name__ == '__main__':

    unittest.main()