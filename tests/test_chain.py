import unittest
import numpy as np
from nexpy.core.basic import Chain

""" Test the Chain object. 

Notes:
------
1. The tests are not exhaustive."""

class TestChain(unittest.TestCase):

    def test_element_getter(self):

        """ Test the element getter of the chain object. """

        element = np.random.rand()

        chain = Chain(element, lambda x: x)

        self.assertEqual(chain.element, element)

    def test_next(self):

        """ Test the next method of the chain object. """

        element = np.random.rand()

        chain = Chain(element, lambda x: x + 1)

        self.assertTrue(chain.next())

        self.assertEqual(chain.element, element + 1)


    def test_react(self):
    
        """ Test the react method of the chain object. """

        element = 0

        chain = Chain(element, lambda x: None if x >= 10 else x + 1)

        self.assertEqual(chain.react(), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])


if __name__ == '__main__':

    unittest.main()