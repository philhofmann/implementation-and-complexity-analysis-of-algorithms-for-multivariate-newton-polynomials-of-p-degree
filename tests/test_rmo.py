import unittest
import numpy as np
from nexpy.core.triangular import RowMajorOrdering
from nexpy.global_settings import FLOAT_DTYPE, EPS_LOW, EPS

""" Test the RowMajorOrdering class. 

Notes:
------
1. The tests are not exhaustive."""

class TestRowMajorOrdering(unittest.TestCase):

    def test_entries_getter(self):

        n = np.random.randint(1, 100)

        data = np.random.rand(n).astype(FLOAT_DTYPE)

        rmo = RowMajorOrdering(data)

        self.assertTrue(np.array_equal(rmo.entries, data))

    def test_item(self):

        n = np.random.randint(1, 100)

        data = np.random.rand(n, n).astype(FLOAT_DTYPE)

        rmo = RowMajorOrdering(data)

        tril = np.tril(data)

        for i in range(n):

            for j in range(n):

                value = rmo.item(i, j)

                self.assertEqual(tril[i][j], value)

    def test_upper_sub_matrix(self):
            
        n = np.random.randint(1, 100)

        data = np.random.rand(n, n).astype(FLOAT_DTYPE)

        rmo = RowMajorOrdering(data)

        tril = np.tril(data)

        for i in range(n):

            sub = rmo.upper_sub_matrix(i)

            self.assertTrue(np.array_equal(sub, tril[:i, :i]))

    def test_dot(self):
            
        n = np.random.randint(1, 100)

        data = np.random.rand(n, n).astype(FLOAT_DTYPE)

        rmo = RowMajorOrdering(data)

        vec = np.random.rand(n).astype(FLOAT_DTYPE)

        res = rmo.dot(vec)

        tril = np.tril(data)

        eps = EPS(res, tril.dot(vec))

        self.assertTrue(eps < EPS_LOW)

    def test_reconstruct(self):
            
        n = np.random.randint(1, 100)

        data = np.random.rand(n, n).astype(FLOAT_DTYPE)

        rmo = RowMajorOrdering(data)

        rec = rmo.reconstruct()

        tril = np.tril(data)

        self.assertTrue(np.array_equal(rec, tril))

if __name__ == '__main__':

    unittest.main()