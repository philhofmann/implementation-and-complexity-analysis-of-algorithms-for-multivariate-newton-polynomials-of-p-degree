import unittest
import numpy as np
from nexpy.core.triangular import Index, IntegerList

""" Test the Index class. 

Notes:
------
1. The tests are not exhaustive.
2. We do not test to string methods.
3. We do not test whether the operations are all correctly fed into the history."""

class TestIndex(unittest.TestCase):

    def test_value_getter(self):

        """Test the value getter of the index object."""

        value = np.random.randint(1, 100)

        index = Index(value)

        self.assertEqual(index.value, value)

    def test_value_setter(self):

        """Test the value setter of the index object."""

        value = np.random.randint(1, 100)

        index = Index(value)

        new_value = np.random.randint(1, 100)

        index.value = new_value

        self.assertEqual(index.value, new_value)

    def test_history_empty_getter(self):

        """Test the history getter of the index object when the history is empty."""

        value = np.random.randint(1, 100)

        index = Index(value)

        self.assertTrue(index.history.size == 0)

    def test_append_history(self):

        """Test the append history method of the index object."""

        value = np.random.randint(1, 100)

        index = Index(value)

        new_value = np.random.randint(1, 100)

        index._append_history(new_value)

        self.assertEqual(index.history, np.array([new_value], dtype = np.int64))

    def test_copy_history(self):

        """Test the copy history method of the index object."""

        value = np.random.randint(1, 100)

        index = Index(value)

        new_value = np.random.randint(1, 100)

        index._append_history(new_value)

        index_copy = Index(0)

        index_copy._copy_history(index.history)

        self.assertEqual(index_copy.history, index.history)

    def test_plusplus(self):

        """Test the plusplus method of the index object."""

        value = np.random.randint(1, 100)

        index = Index(value)

        index.plusplus()

        self.assertEqual(index.value, value + 1)
    
    def test_reduction(self): 

        """Test the index reduction method of the index object."""

        L = IntegerList(np.array([3, 2, 1, 2, 1, 1, 2, 1, 1, 1]))

        correct = [0, 0, 0, 1, 1, 2, 3, 3, 4, 5, 6, 6, 7, 8, 9]

        for idx_L in range(L.size):

            value = idx_L

            index = Index(value)

            index_reduction = index.reduction(L)

            self.assertEqual(index_reduction.value, correct[idx_L])

    def test_relative(self):

        """Test the relative method of the index object."""

        L = IntegerList(np.array([3, 2, 1, 2, 1, 1, 2, 1, 1, 1]))

        correct = [0, 1, 2, 0, 1, 0, 0, 1, 0, 0]

        for idx_L in range(L.length):

            value = idx_L

            index = Index(value)

            index_relative = index.relative(L)

            self.assertEqual(index_relative.value, correct[idx_L])

    def test_restoration(self):

        """Test the restoration method of the index object."""

        L = IntegerList(np.array([3, 2, 1, 2, 1, 1, 2, 1, 1, 1]))

        correct = np.array([
            [0, 0, 0, 0],
            [1, 0, 0, 0],
            [2, 0, 0, 0],
            [0, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 2, 0, 0],
            [0, 0, 1, 0],
            [1, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 0, 2, 0],
            [0, 0, 0, 1],
            [1, 0, 0, 1],
            [0, 1, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 0, 2]
        ])

        for idx_L in range(L.length):

            value = idx_L

            index = Index(value)

            restoration = index.restoration(L, 4)

            self.assertTrue(np.array_equal(restoration, correct[idx_L]))

if __name__ == '__main__':

    unittest.main()
