import unittest
import numpy as np
from nexpy.global_settings import EPS_LOW, EPS
from nexpy.random_utils import deep_compatible_integer_list, ltfbm

""" Test the LowerTriangularFactorisedBlockMatrix class. 

Notes:
------
1. The tests are not exhaustive
2. We restrict to complex class methods only
3. n*min_entropy < min_max_entropy in order to get integer lists with at least depth 2
4. When choosing min_entropy = 1, this means n should be smaller than min_max_entropy
5. Choosing min_entropy = 1 also means that the integer list is fully reductable"""

class TestLowerTriangularFactorisedBlockMatrix(unittest.TestCase):

    def test_reconstruct(self):

        """ Test the ltfbm reconstruct method. 
        
        Notes:
        ------
        planned """

        pass

    def test_item(self):

        """ Test the ltfbm item method.
        
        Notes:
        ------
        1. Assumes that the ltfbm reconstruct is working correctly
        2. Compares the reconstruction method with the item method"""

        for _ in range(10):

            depth = np.random.randint(2, 4)

            tiling = deep_compatible_integer_list(depth=depth) # is fully reducible bot doesn't need to be !

            size = tiling.size

            rand_ltfbm = ltfbm(tiling) # is fully factorised

            mat = np.zeros((size, size))

            for row in range(size):

                for col in range(size):

                    mat[row][col] = rand_ltfbm.item(row=row, col=col)

            rec = rand_ltfbm.reconstruct().reconstruct()

            eps = EPS(rec, mat)

            self.assertTrue(eps < EPS_LOW)


    def test_dot_deep_compatible(self):

        """ Test the fast ltfbm dot method.
        
        Notes:
        ------
        1. Assumes that the ltfbm reconstruct is working correctly
        2. Compares the reconstruction method with the fast dot method
        3. Note that the fast dot fast method is designed for *deep* compatible integer lists *only*"""

        for _ in range(10):

            depth = np.random.randint(2, 5)

            tiling = deep_compatible_integer_list(depth=depth) # is fully reducible and need to be !

            size = tiling.size

            rand_ltfbm = ltfbm(tiling) # is fully factorised

            # depth of ltfbm format and tiling thus coincides tiling is deep compatible anyways

            vec = np.random.rand(size)

            dot = rand_ltfbm.dot(vec, deep_compatible=True)

            rec = rand_ltfbm.reconstruct().reconstruct()

            eps = EPS(rec.dot(vec), dot)

            self.assertTrue(eps < EPS_LOW)

    def test_dot_compatible(self):

        """ Test the deep ltfbm dot method.
        
        Notes:
        ------
        1. Assumes that the ltfbm reconstruct is working correctly
        2. Compares the reconstruction method with the deep dot method
        3. Note that the dot deep method is designed for compatible integer lists *only*"""

        for _ in range(10):

            depth = np.random.randint(2, 5)

            tiling = deep_compatible_integer_list(depth=depth) # is fully reducible bot doesn't need to be !

            size = tiling.size

            rand_ltfbm = ltfbm(tiling) # is fully factorised 

            # depth of ltfbm format and tiling thus coincides tiling is compatible anyways

            vec = np.random.rand(size)

            dot = rand_ltfbm.dot(vec)

            rec = rand_ltfbm.reconstruct().reconstruct()

            eps = EPS(rec.dot(vec), dot)

            self.assertTrue(eps < EPS_LOW)

if __name__ == '__main__':

    unittest.main()

    # TODO: create notebook with ltfbm(..., depth = 2)