import unittest
import numpy as np

from nexpy.global_settings import INT_DTYPE
from nexpy.random_utils import deep_compatible_integer_list
from nexpy.core.triangular import DeepCompatibleIntegerList

""" Test the DeepCompatibleIntegerList class. 

Notes:
------
1. The tests are not exhaustive. 
2. We do not test whether the operations are all correctly fed into the history."""

class TestDeepCompatibleIntegerList(unittest.TestCase):

    
    def test_cumsum_getter(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        cumsum = np.cumsum(tiling)

        dcil.init(tiling, depth)

        self.assertTrue(np.array_equal(dcil.cumsum, cumsum))


    def test_cumsum_2d_mask_getter(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        cumsum = np.cumsum(tiling)

        dcil.init(tiling, depth)

        self.assertEqual(dcil.cumsum_2d_mask[0][0], 0)

        self.assertTrue(np.array_equal(dcil.cumsum_2d_mask[:,0][1:], cumsum[0:-1]))

        self.assertTrue(np.array_equal(dcil.cumsum_2d_mask[:,1], cumsum))

    
    def test_length_getter(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        dcil.init(tiling, depth)

        self.assertEqual(dcil.length, len(tiling))


    def test_depth_getter(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        dcil.init(tiling, depth)

        self.assertEqual(dcil.depth, depth)

    
    def test_indices_getter(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        dcil.init(tiling, depth)

        self.assertTrue(np.array_equal(dcil.indices, np.zeros((max([depth - 1, 1])), dtype = INT_DTYPE)))

    
    def test_bunch(self):

        dcil = DeepCompatibleIntegerList()

        depth = np.random.randint(2, 5)

        tiling = deep_compatible_integer_list(depth = depth).entries

        length = len(tiling)

        dcil.init(tiling, depth)

        for i in range(length):

            self.assertEqual(dcil.element, tiling[i])

            self.assertEqual(dcil.index, i)

            dcil.next()

        self.assertTrue(dcil._precomp)

        dcil.clear()

        for i in range(length):

            self.assertEqual(dcil.element, tiling[i])

            dcil.next()

if __name__ == '__main__':

    unittest.main()
