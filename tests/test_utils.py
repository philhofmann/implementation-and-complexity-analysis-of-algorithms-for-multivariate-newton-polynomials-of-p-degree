import unittest
import numpy as np
from nexpy.utils import get_exponent_matrix_conditional, extract_tiling_from_exponent_matrix
from nexpy.jit_compiled_utils import get_exponent_matrix_iterative_subroutine, get_exponent_matrix_iterative_tilings_only_subroutine

""" Test the utils module.

Notes:
------
1. The tests are not exhaustive.
2. Test coverage low."""

class TestUtils(unittest.TestCase):

    def test_get_exponent_matrix_iterative_subroutine(self):

        for _ in range(1000):

            m, n, p = np.random.randint(1, 4), np.random.randint(1, 10), np.random.rand()*2.0

            gem_iter = get_exponent_matrix_iterative_subroutine(m, n, p)

            gem_cond = get_exponent_matrix_conditional(m, n, p)

            self.assertTrue(np.array_equal(gem_iter, gem_cond))

    def test_get_exponent_matrix_iterative_tilings_only_subroutine(self):

        for _ in range(1000):

            m, n, p = np.random.randint(2, 4), np.random.randint(1, 10), np.random.rand()*2.0

            gem_iter = get_exponent_matrix_iterative_tilings_only_subroutine(m, n, p)

            gem_cond = extract_tiling_from_exponent_matrix(get_exponent_matrix_conditional(m, n, p))

            self.assertTrue(np.array_equal(gem_iter, gem_cond))

if __name__ == "__main__":

    unittest.main()